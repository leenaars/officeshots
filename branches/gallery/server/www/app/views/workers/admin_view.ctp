<div class="workers view">
<h2><?php  __('Worker');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $worker['Worker']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Factory'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($worker['Factory']['name'], array('controller'=> 'factories', 'action'=>'view', $worker['Factory']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Application'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($worker['Application']['name'], array('controller'=> 'applications', 'action'=>'view', $worker['Application']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Version'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $worker['Worker']['version']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $worker['Worker']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $worker['Worker']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Worker', true), array('action'=>'edit', $worker['Worker']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Worker', true), array('action'=>'delete', $worker['Worker']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $worker['Worker']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Workers', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Worker', true), array('action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Factories', true), array('controller'=> 'factories', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Factory', true), array('controller'=> 'factories', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Applications', true), array('controller'=> 'applications', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Application', true), array('controller'=> 'applications', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Formats', true), array('controller'=> 'formats', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Format', true), array('controller'=> 'formats', 'action'=>'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Formats');?></h3>
	<?php if (!empty($worker['Format'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Code'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($worker['Format'] as $format):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $format['id'];?></td>
			<td><?php echo $format['name'];?></td>
			<td><?php echo $format['code'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller'=> 'formats', 'action'=>'view', $format['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller'=> 'formats', 'action'=>'edit', $format['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller'=> 'formats', 'action'=>'delete', $format['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $format['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Format', true), array('controller'=> 'formats', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
