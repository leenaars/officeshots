<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Model', 'Result');

/**
 * A helper to assist in outputting Requests
 */
class ResultModelHelper extends AppHelper
{
	/**
	 * Convert Result state to a human readable message
	 *
	 * @param array $request an array containing the request
	 * @return string the state message
	 */
	public function getState($result) {
		if (!is_array($result)) {
			return '';
		}

		if (!isset($result['Result'])) {
			$result = array('Result' => $result);
		}
		
		switch ($result['Result']['state']) {
		case Result::STATE_UPLOADING:
			return $this->output(__('Uploading', true));
		case Result::STATE_SCAN_QUEUED:
			return $this->output(__('Queued for virus scan', true));
		case Result::STATE_SCAN_FOUND:
			return $this->output(sprintf(__('Scan failed. File infected with "%s"', true), $result['Result']['state_info']));
		case Result::STATE_SCAN_FAILED:
			return $this->output(sprintf(__('Scan failed with error "%s"', true), $result['Result']['state_info']));
		case Result::STATE_FINISHED:
			return $this->output(__('Finished', true));
		default:
			return $this->output(__('Unknown', true));
		}
	}
}

?>
