<div class="jobs index">
<h2><?php __('Jobs');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('request_id');?></th>
	<th><?php echo $paginator->sort('platform_id');?></th>
	<th><?php echo $paginator->sort('application_id');?></th>
	<th><?php echo $paginator->sort('version');?></th>
	<th><?php echo $paginator->sort('result_id');?></th>
	<th><?php echo $paginator->sort('factory_id');?></th>
	<th><?php echo $paginator->sort('locked');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('updated');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($jobs as $job):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $job['Job']['id']; ?>
		</td>
		<td>
			<?php echo $html->link($job['Request']['filename'], array('controller'=> 'requests', 'action'=>'view', $job['Request']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($job['Platform']['name'], array('controller'=> 'platforms', 'action'=>'view', $job['Platform']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($job['Application']['name'], array('controller'=> 'applications', 'action'=>'view', $job['Application']['id'])); ?>
		</td>
		<td>
			<?php echo $job['Job']['version']; ?>
		</td>
		<td>
			<?php echo $html->link($job['Result']['filename'], array('controller'=> 'results', 'action'=>'view', $job['Result']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($job['Factory']['name'], array('controller'=> 'factories', 'action'=>'view', $job['Factory']['id'])); ?>
		</td>
		<td>
			<?php echo $job['Job']['locked']; ?>
		</td>
		<td>
			<?php echo $job['Job']['created']; ?>
		</td>
		<td>
			<?php echo $job['Job']['updated']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $job['Job']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $job['Job']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $job['Job']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $job['Job']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Job', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('List Requests', true), array('controller'=> 'requests', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Request', true), array('controller'=> 'requests', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Applications', true), array('controller'=> 'applications', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Application', true), array('controller'=> 'applications', 'action'=>'add')); ?> </li>
	</ul>
</div>
