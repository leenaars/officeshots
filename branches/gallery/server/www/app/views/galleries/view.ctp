<div class="galleries view">
	<h2><?php echo $gallery['Gallery']['name'];?></h2>
	<?php echo $gallery['Gallery']['description_html'];?>
</div>

<div class="related">
	<h3><?php __('Documents');?></h3>
	<?php if (!empty($gallery['Request'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Filename'); ?></th>
		<th><?php __('Created by'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('State'); ?></th>
		<th><?php __('Results'); ?></th>
		<th class="actions"><?php __('Actions'); ?></th>
	</tr>
	<?php foreach ($gallery['Request'] as $request):?>
		<tr>
			<td><?php echo $html->link($request['filename'], array('controller'=> 'requests', 'action'=>'view', $request['id'])); ?></td>
			<td><?php echo $html->link($request['User']['name'], array('controller' => 'users', 'action' => 'view', $request['User']['id']));?></td>
			<td><?php echo $request['created'];?></td>
			<td><?php echo $requestModel->getState($request); ?></td>
			<td><?php echo $request['result_count'] . '/' . $request['job_count'];?></td>
			<td class="actions">
				<?php echo $html->link(__('Remove', true), array('action' => 'remove_document', $gallery['Gallery']['slug'], $request['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Add a document', true), array('action'=>'add_document', $gallery['Gallery']['slug'])); ?> </li>
		<li><?php echo $html->link(__('Edit this gallery', true), array('action'=>'edit', $gallery['Gallery']['slug'])); ?> </li>
		<li><?php echo $html->link(__('Delete this gallery', true), array('action'=>'delete', $gallery['Gallery']['slug']), null, sprintf(__('Are you sure you want to delete "%s"?', true), $gallery['Gallery']['name'])); ?> </li>
	</ul>
</div>
