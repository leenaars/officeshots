<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Make a model associate with a file on the filesystem
 */
class FileBehavior extends ModelBehavior
{
	/** @var array Default settings of the behaviour */
	public $settings = array(
		// Mappings for the database fields
		'fields' => array('filename' => 'filename', 'path' => 'path', 'mimetype_id' => 'mimetype_id'),
		// Location that the files will be stored
		'directory' => '',
		// Set to true to save the model record even when there were file errors
		'saveOnError' => false,
		// The attribute in the model that contains an array of valid mimetypes
		'mimetypes' => 'mimetypes',
		// A custom validation method in the model
		'customValidate' => null
	);

	/** @var array The errors that were encountered */
	public $errors = array();

	/**
	 * Initialize the File behaviour
	 *
	 * @param object $model A reference to the model
	 * @param mixed config The configuration of the behaviour
	 * @return void
	 */
	public function setup(&$model, $config = array())
	{
		if (!is_array($config)) {
			$config = array('directory' => $config);
		}

		$this->settings = array_merge($this->settings, $config);

		if (!$this->settings['directory']) {
			$this->settings['directory'] = 'files';
		}

		$cwd = getcwd();
		chdir(APP);
		$this->settings['directory'] = realpath($this->settings['directory']);
		chdir($cwd);

		if (!is_writeable($this->settings['directory'])) {
			$this->errors[] = __('File storage directory does not exist.', true);
			return;
		}

		if (!is_writeable($this->settings['directory'])) {
			$this->errors[] = __('File storage directory is not writeable.', true);
			return;
		}

		// Permanently bind the Mimetype model
		$model->bindModel(array('belongsTo' => array('Mimetype')), false);
	}

	/**
	 * Set file records to an uploaded file
	 *
	 * @param object $model A reference to the model
	 * @param array $file The file structure from the controller data
	 * @return boolean True on success or false on failure
	 */
	public function setFileUpload(&$model, $file = array())
	{
		if (!is_array($file) || sizeof($file) == 0) {
			$this->errors[] = __('There was no uploaded file', true);
			return false;
		}

		if (sizeof($file) == 1) {
			$file = array_shift($file);
			if (!is_array($file) || sizeof($file) == 0) {
				$this->errors[] = __('There was no uploaded file', true);
				return false;
			}
		}

		if (!$file['name'] || !isset($file['type']) || !$file['size'] || !$file['tmp_name'] || $file['error'] !== 0) {
			$this->errors[] = __('There was an error uploading the file.', true);
			return false;
		}

		if (!is_uploaded_file($file['tmp_name'])) {
			$this->errors[] = __('File is not an uploaded file. Attempted security breach?', true);
			return false;
		}

		if (!$this->deleteFile($model)) {
			$this->log(__('File is not an uploaded file. Attempted security breach?', true));
			return false; // Error has been set by deleteFile()
		}

		$mimestring = '';
		if (!$mimetype = $this->_getMimetype($model, $file['tmp_name'], $mimestring)) {
			$this->errors[] = sprintf(__('Invalid mimetype %s given for uploaded file', true), $mimestring);
			return false;
		}

		if (!$this->_checkMimetype($mimetype)) {
			$this->errors[] = __('Illegal mimetype.', true);
			return false;
		}

		$destination = $this->_generatePath();
		if (!move_uploaded_file($file['tmp_name'], $this->settings['directory'] . DS . $destination)) {
			$this->errors[] = __('The uploaded file cannot be moved.', true);
			return false;
		}

		$model->set(array(
			$this->settings['fields']['filename'] => $file['name'],
			$this->settings['fields']['mimetype_id'] => $mimetype['Mimetype']['id'],
			$this->settings['fields']['path'] => $destination
		));
		
		return true;
	}

	/**
	 * Set the file records to any file
	 *
	 * @param object $model A reference to the model
	 * @param string $path full path to the file
	 * @param boolean $move True if the file should be moved, false if it should be copied.
	 * @return boolean True on success or false on failure
	 */
	public function setFile(&$model, $path = false, $move = false)
	{
		if (!$path) {
			$this->errors[] = __('There was no file', true);
			return false;
		}

		if (!is_readable($path)) {
			$this->errors[] = __('The file cannot be read.', true);
			return false;
		}

		$mimestring = '';
		if (!$mimetype = $this->_getMimetype($model, $path, $mimestring)) {
			$this->errors[] = sprintf(__('Invalid mimetype %s detected for file', true), $mimestring);
			return false;
		}

		if (!$this->_checkMimetype($mimetype)) {
			$this->errors[] = __('Illegal mimetype.', true);
			return false;
		}

		$filename = basename($path);
		$destination = $this->_generatePath();
		if ($move) {
			$result = rename($path, $this->settings['directory'] . DS . $destination);
		} else {
			$result = copy($path, $this->settings['directory'] . DS . $destination);
		}

		if (!$result) {
			$this->errors = __('The file could not be copied or moved.');
			return false;
		}

		$model->set(array(
			$this->settings['fields']['filename'] => $filename,
			$this->settings['fields']['mimetype_id'] => $mimetype['Mimetype']['id'],
			$this->settings['fields']['path'] => $destination
		));
		
		return True;
	}

	/**
	 * Set the file records to a file buffer
	 *
	 * @param object $model A reference to the model
	 * @param string $buffer A reference to a string holding the file contents
	 * @param string $filename The filename of the file
	 * @param mixed $filters a stream filter or array of stream filters to apply
	 */
	public function setFileBuffer(&$model, &$buffer, $filename, $filters = false)
	{
		$destination_file = $this->_generatePath();
		$destination_path = $this->settings['directory'] . DS . $destination_file;

		if (!$file = fopen($destination_path, 'w')) {
			$this->errors[] = __('Cannot write file from buffer.', true);
			return false;
		}

		if ($filters) {
			if (!is_array($filters)) {
				$filters = array($filters);
			}

			foreach ($filters as $filter) {
				stream_filter_append($file, $filter, STREAM_FILTER_WRITE);
			}
		}

		fwrite($file, $buffer);
		fclose($file);
		
		$mimestring = '';
		if (!$mimetype = $this->_getMimetype($model, $destination_path, $mimestring)) {
			$this->errors[] = sprintf(__('Invalid mimetype %s detected for file buffer', true), $mimestring);
			unlink($destination_path);
			return false;
		}

		if (!$this->_checkMimetype($mimetype)) {
			$this->errors[] = __('Illegal mimetype.', true);
			unlink($destination_path);
			return false;
		}
		
		$model->set(array(
			$this->settings['fields']['filename'] => $filename,
			$this->settings['fields']['mimetype_id'] => $mimetype['Mimetype']['id'],
			$this->settings['fields']['path'] => $destination_file
		));
		
		return true;
	}

	/**
	 * Delete the currrent on-disk file, if any
	 *
	 * @param object $model A reference to the model
	 * @return True on success, false on failure
	 */
	public function deleteFile(&$model)
	{
		if (!$model->id) {
			return true; // There is no model instance, so there is no file
		}

		$path = $model->field('path');
		if (!$path && !is_file($this->settings['directory'] . DS . $path)) {
			return true; // There is no file
		}

		if (!is_writeable($this->settings['directory'] . DS . $path)) {
			$this->errors[] = __('The file cannot be deleted.', true);
			return false;
		}

		return unlink($this->settings['directory'] . DS . $path);
	}

	/**
	 * Return the contents of the file
	 *
	 * @param object $model A reference to the model
	 * @return string The file contents or null if there is no file or it cannot be read.
	 */
	public function getContents(&$model)
	{
		if (!isset($model->id)) {
			return null;
		}
		
		$path = $model->field('path');
		if (!$path && !is_file($this->settings['directory'] . DS . $path)) {
			return null; // There is no file
		}

		return file_get_contents($this->settings['directory'] . DS . $path);
	}

	/**
	 * Return the full filesystem path to the file, or false if there is no file
	 *
	 * @param object $model A reference to the model
	 * @return string full path to the file
	 */
	public function getPath(&$model)
	{
		if (!isset($model->id)) {
			return false;
		}
		
		$path = $model->field($this->settings['fields']['path']);
		if (!$path && !is_file($this->settings['directory'] . DS . $path)) {
			return false; // There is no file
		}

		return $this->settings['directory'] . DS . $path;
	}

	/**
	 * Generate a new full path to write to
	 */
	protected function _generatePath()
	{
		return String::uuid();
	}

	/**
	 * Return the Mimetype for a mimetype string
	 *
	 * @param string $path The path to a file to get the mimetype from
	 * return array The Mimetype search result
	 */
	protected function _getMimetype(&$model, $path, &$mimestring = '')
	{
		$finfo = finfo_open(FILEINFO_MIME);
		$mimestring = finfo_file($finfo, $path);
		finfo_close($finfo);

		$mimetype = $model->Mimetype->find('first', array(
			'conditions' => array(
				'Mimetype.name' => $mimestring,
			),
		));

		return $mimetype;
	}

	/**
	 * Check mimeinfo of the file
	 *
	 * TODO: Implement this. It's a dummy now
	 * @param array $mimetype The mimetype to check
	 * @return boolean True if the mimetype is valid, False otherwise
	 */
	protected function _checkMimetype($mimetype)
	{
		return true;
	}

	/**
	 * Abort the save if there were errors with the file records
	 *
	 * @param object $model A reference to the model
	 * @return True on success, false to abort the save
	 */
	public function beforeSave(&$model)
	{
		if (!isset($model->data[$model->name][$this->settings['fields']['path']])) {
			return true; // No file is being saved
		}

		$filename = $model->data[$model->name][$this->settings['fields']['path']];

		if ($filename && !file_exists($this->settings['directory'] . DS . $filename)) {
			$this->errors[] = __('The filename field is set but the file does not exist', true);
		}

		if (!empty($this->errors) && !$this->settings['saveOnError']) {
			return false;
		}

		return true;
	}

	/**
	 * Delete the file when the record is deleted
	 *
	 * @param object $model A reference to the model
	 * @return True on success, false to abort the deletion
	 */
	public function beforeDelete(&$model)
	{
		$filename = $this->settings['directory'] . DS . $model->field($this->settings['fields']['path']); 
		if (file_exists($filename)) {
			if (!unlink($filename)) {
				$this->errors[] = __('The file could not be deleted.', true);
			}
		}
		
		if (!empty($this->errors) && !$this->settings['saveOnError']) {
			return false;
		}

		return true;
	}
}
