<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Controller for the Groups model
 *
 * This controller only contains admin functions.
 */
class GroupsController extends AppController
{
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form');

	/** @var array The components this controller uses */
	public $components = array('AuthCert');

	/** @var array The models that this controller uses */
	public $uses = array('Group', 'GroupsUser');

	/**
	 * List all the groups
	 *
	 * @return void
	 */
	public function admin_index()
	{
		$this->paginate = array(
			'contain' => array('User'),
		);
		$this->set('groups', $this->paginate());
	}

	/**
	 * View a single group and edit it's members and permissions
	 *
	 * @return void
	 */
	public function admin_view($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Group.', true));
			$this->redirect(array('action'=>'index'));
		}

		$this->set(array(
			'group' => $this->Group->read(null, $id),
		));
	}

	/**
	 * Create a new group
	 *
	 * @return void
	 */
	public function admin_add()
	{
		if (!empty($this->data)) {
			$this->Group->create();
			if ($this->Group->save($this->data)) {
				$this->Session->setFlash(__('The Group has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Group could not be saved. Please, try again.', true));
			}
		}

		$this->render('admin_edit');
	}

	/**
	 * Edit a group
	 *
	 * @return void
	 */
	public function admin_edit($id = null)
	{
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Group', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Group->save($this->data)) {
				$this->Session->setFlash(__('The Group has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Group could not be saved. Please, try again.', true));
			}
		} else {
			$this->data = $this->Group->read(null, $id);
		}
	}

	/**
	 * Delete a group
	 *
	 * @return void
	 */
	public function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Group', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Group->del($id, true)) {
			$this->Session->setFlash(__('Group deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

	/**
	 * Remove a member from the group
	 *
	 * @param string $id The ID of the group
	 * @param string $user_id The ID of the user
	 * @return void
	 */
	public function admin_removeMember($id = null, $user_id = null)
	{
		if (!$id || !$user_id) {
			$this->Session->setFlash(__('Invalid group membership', true));
			$this->redirect(array('action'=>'index'));
		}

		$relation = $this->GroupsUser->find('first', array('conditions' => array(
			'group_id' => $id,
			'user_id' => $user_id,
		)));

		if (!empty($relation)) {
			if ($this->GroupsUser->del($relation['GroupsUser']['id'])) {
				$this->Session->setFlash(__('The member has been removed', true));
				$this->redirect(array('action'=>'view', $id));
			}
		}
		
		$this->Session->setFlash(__('Invalid group membership', true));
		$this->redirect(array('action'=>'view', $id));
	}
}
?>
