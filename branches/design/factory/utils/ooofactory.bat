set LOCALPYTHON=C:\Python26
set OOOPATH=C:\Program Files\OpenOffice.org 3

set PYTHONPATH=%LOCALPYTHON%\DLLs
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\plat-win
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\tk
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\site-packages

cd ..\src
"%OOOPATH%\program\python.exe" factory.py %*
