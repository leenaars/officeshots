<div class="platforms form">
<?php echo $form->create('Platform');?>
	<fieldset>
 		<legend><?php __('Edit Platform');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('name');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Platform.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Platform.id'))); ?></li>
		<li><?php echo $html->link(__('List Platforms', true), array('action'=>'index'));?></li>
	</ul>
</div>
