<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A helper to assist in outputting Jobs
 */
class JobModelHelper extends AppHelper
{
	/** @var array Add the Time helper */
	public $helpers = array('Html');

	/**
	 * Return the description of a Job
	 */
	public function getDescription($job, $request_state)
	{
		if (!isset($job['Job'])) {
			$job = array('Job' => $job);
		}

		if (!empty($job['Job']['Result'])) {
			return $this->output($job['Job']['Result']['Format']['name']);
		}

		if ($request_state & (Request::STATE_UPLOADING | Request::STATE_SCAN_QUEUED | Request::STATE_QUEUED)) {
			if ($job['Job']['locked'] == '0000-00-00 00:00:00') {
				return $this->output(__('Queued'));
			}

			if (empty($job['Job']['Result'])) {
				return $this->output(__('Processing'));
			}
		}

		return $this->output(__('Failed', true));
	}

	/**
	 * Return the icon of a Job
	 */
	public function getIcon($job, $request_state)
	{
		if (!isset($job['Job'])) {
			$job = array('Job' => $job);
		}

		if (!empty($job['Job']['Result'])) {
			if ($job['Job']['Result']['state'] == Result::STATE_SCAN_FOUND) {
				$icon = 'virus.png';
			} else {
				$icon = $job['Job']['Result']['Mimetype']['icon'];
			}

			return $this->output($this->Html->image('icons/' . $icon, array(
				'alt' => $job['Job']['Result']['Format']['name'],
				'url' => array('controller'=> 'results', 'action'=>'view', $job['Job']['Result']['id'])
			)));
		}

		if ($request_state == Request::STATE_QUEUED) {
			if ($job['Job']['locked'] == '0000-00-00 00:00:00') {
				return $this->output($this->Html->image('icons/queued.png', array('alt' => __('Queued', true))));
			}

			if (empty($job['Job']['Result'])) {
				return $this->output($this->Html->image('icons/busy.png', array('alt' => __('Processing', true))));
			}
		}

		return $this->output($this->Html->image('icons/error.png', array('alt' => __('Failed', true))));
	}
}

?>
