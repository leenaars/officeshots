<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Application model
 *
 * An Application is a specific instance of an office suite application running
 * on a specific factory.
 */
class Worker extends AppModel
{
	/** @var string Every application runs on a factory and belongs to an application type */
	public $belongsTo = array('Factory', 'Application');

	/** @var array Every application supports one or more output formats */
	public $hasAndBelongsToMany = array(
		'Format' => array('unique' => true)
	);

	/** @var array Job queries can be quite complex, so use Containable */
	public $actsAs = array('Containable');

	/**
	 * Set validation rules in here so we can have i18n messages
	 */
	public function beforeValidate()
	{
		$this->validate = array(
			'version' => array(
				'rule' => array('custom', '/^[a-z0-9(). -]{1,}$/i'),
				'message' => __('Only letters, numbers, dashes, parenthesis, the dot (.) and spaces allowed.', true)
			)
		);

		return True;
	}

	/**
	 * Get a list of all active worker types. That is, unique combinations of Platform, Doctype and Application
	 *
	 * @param integer $time Maximum time since the last poll, in minutes
	 * @return array
	 */
	public function getActive()
	{
		App::import('Sanitize');

		$time = date('Y-m-d H:i:s', time() - Configure::read('Factory.polltime'));
		$active = $this->query("SELECT DISTINCT
				`Worker`.`version`,
				`Worker`.`development`,
				`Platform`.`id`,
				`Platform`.`name`,
				`Doctype`.`id`,
				`Doctype`.`name`,
				`Doctype`.`code`,
				`Application`.`id`,
				`Application`.`name`
			FROM `workers` AS `Worker`
			LEFT JOIN `factories` AS `Factory` ON (`Worker`.`factory_id` = `Factory`.`id`)
			LEFT JOIN `operatingsystems` AS `Operatingsystem` ON (`Factory`.`operatingsystem_id` = `Operatingsystem`.`id`)
			LEFT JOIN `platforms` AS `Platform` ON (`Operatingsystem`.`platform_id` = `Platform`.`id`)
			LEFT JOIN `applications` AS `Application` ON (`Worker`.`application_id` = `Application`.`id`)
			LEFT JOIN `applications_doctypes` AS `ApplicationsDoctype` ON `Application`.`id` = `ApplicationsDoctype`.`application_id`
			LEFT JOIN `doctypes` AS `Doctype` ON (`ApplicationsDoctype`.`doctype_id` = `Doctype`.`id`)
			WHERE `Factory`.`last_poll` > '$time'
			ORDER BY `Platform`.`name` ASC, `Application`.`name` ASC, `Worker`.`version` ASC");

		if (is_array($active)) {
			foreach ($active as &$app) {
				$app['id'] = $app['Platform']['id'] . '_' . $app['Doctype']['code'] . '_' . $app['Application']['id'] . '_'
				           . $app['Worker']['version'];

				$formats = $this->query("SELECT DISTINCT
						`Format`.`id`,
						`Format`.`code`
					FROM `workers` AS `Worker`
					LEFT JOIN `formats_workers` AS `FormatsWorker` ON (`Worker`.`id` = `FormatsWorker`.`worker_id`)
					LEFT JOIN `formats` AS `Format` ON (`FormatsWorker`.`format_id` = `Format`.`id`)
					LEFT JOIN `factories` AS `Factory` ON (`Worker`.`factory_id` = `Factory`.`id`)
					LEFT JOIN `operatingsystems` AS `Operatingsystem` ON (`Factory`.`operatingsystem_id` = `Operatingsystem`.`id`)
					LEFT JOIN `applications` AS `Application` ON (`Worker`.`application_id` = `Application`.`id`)
					LEFT JOIN `applications_doctypes` AS `ApplicationsDoctype` ON `Application`.`id` = `ApplicationsDoctype`.`application_id`
					WHERE `Factory`.`last_poll` > '$time'
						AND `Worker`.`version` = '" . Sanitize::escape($app['Worker']['version']) . "'
						AND `Application`.`id` = '" . $app['Application']['id'] . "'
						AND `Operatingsystem`.`platform_id` = '" . $app['Platform']['id'] . "'
						AND `ApplicationsDoctype`.`doctype_id` = '" . $app['Doctype']['id'] . "'");

				$app['Format'] = array();
				foreach ($formats as $format) {
					$app['Format'][] = $format['Format'];
				}
			}
			return $active;
		}
		return array();
	}

	/**
	 * Get a list of all worker application names and versions, grouped by ODF doctype and platform
	 *
	 * @param integer $time Maximum time since the last poll, in minutes
	 * @return array A nested array
	 */
	public function getActiveGrouped($time = 10)
	{
		$result = array();
		$doctypes = $this->Application->Doctype->find('all');
		$platforms = $this->Factory->Operatingsystem->Platform->find('all');

		foreach ($doctypes as $doctype) {
			foreach ($platforms as $platform) {
				$result[$doctype['Doctype']['name']][$platform['Platform']['name']] = array();
			}
		}

		$active = $this->getActive();
		return $active;
	}
}

?>
