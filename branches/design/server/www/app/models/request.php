<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Core', 'Clamd.Clamd');

/**
 * The Request model
 */
class Request extends AppModel
{
	/**#@+
	 * Request states
	 */
	const STATE_UPLOADING   = 1;
	const STATE_SCAN_QUEUED = 2;
	const STATE_SCAN_FOUND  = 4;
	const STATE_SCAN_FAILED = 8;
	const STATE_QUEUED      = 16;
	const STATE_FINISHED    = 32;
	const STATE_EXPIRED     = 64;
	const STATE_CANCELLED   = 128;
	/**#@-*/

	/** @var array Every request belongs to a user and is associated with a document type and a desired output format */
	public $belongsTo = array('User', 'Format');

	/** @var string A request consists of multiple jobs */
	public $hasMany = 'Job';

	/** @var array Use the file behaviour to associate ODF files with Requests */
	public $actsAs = array('File' => 'files/requests', 'Containable', 'BeanStalk.Deferrable');

	/** @var string Use the filename as the distinguising name */
	public $displayField = 'filename';

	/**
	 * Create a new request with underlying jobs
	 *
	 * If the add fails, &$errors contains the error messages
	 *
	 * @param array $data The data to save if not saving $this->data, including a file and the jobs
	 * @param array &$errors The errors
	 * @return boolean Success
	 */
	public function add($data, &$errors = array())
	{
		if (empty($data)) {
			$errors[] = __('The request was empty.', true);
			return false;
		}
		$this->set($data);
		$this->set(array(
			'expire' => date('Y-m-d H:i:s', time() + Configure::read('Request.expire')),
			'state'  => self::STATE_SCAN_QUEUED
		));

		// Check that we have a file and jobs
		if ((!isset($data['Request']['FileUpload']) || !is_array($data['Request']['FileUpload'])) && !isset($data['Request']['File']) && !isset($data['Request']['FileBuffer'])) {
			$errors[] = __('You did not submit a file.', true);
			return false;
		}

		if (!isset($data['Request']['Job']) || !is_array($data['Request']['Job']) || empty($data['Request']['Job'])) {
			$errors[] = __('You did not select any valid applications', true);
			return false;
		}

		// Add the file
		if (isset($data['Request']['FileUpload'])) {
			if (!$this->setFileUpload($data['Request']['FileUpload'])) {
				$errors[] = __('The file upload failed. Please try again.', true);
				$errors = array_merge($errors, $this->Behaviors->File->errors);
				return false;
			}
		} elseif (isset($data['Request']['File'])) {
			if (!$this->setFile($data['Request']['File'])) {
				$errors[] = __('Adding the file failed. Please try again.', true);
				$errors = array_merge($errors, $this->Behaviors->File->errors);
				return false;
			}
		} else {
			if (!$this->setFileBuffer($data['Request']['FileBuffer']['contents'], $data['Request']['FileBuffer']['filename'])) {
				$errors[] = __('Adding the file buffer failed. Please try again.', true);
				$errors = array_merge($errors, $this->Behaviors->File->errors);
				return false;
			}
		}

		// Save the request
		if (($this->data = $this->save()) === false) {
			$this->deleteFile();
			$errors[] = __('The request could not be saved.', true);
			return false;
		}

		// Find the Mimetype associated with the request
		// We can use this to check the doctype on the submitted jobs below
		$mimetype = $this->Mimetype->find('first', array('conditions' => array(
			'Mimetype.id' => $this->data['Request']['mimetype_id'],
		)));

		// Create jobs for this request
		$jobErrors = 0;
		foreach ($data['Request']['Job'] as $job) {
			$job['request_id'] = $this->id;

			// If the job contains a doctype_code, check it
			if ($job['doctype_code']) {
				if ($job['doctype_code'] != $mimetype['Doctype']['code']) {
					$jobErrors++;
					continue;
				}
			}

			$this->Job->create();
			if (!$this->Job->save(array('Job' => $job))) {
				$jobErrors++;
			}
		}

		if ($jobErrors == sizeof($data['Request']['Job'])) {
			$this->delete();
			$errors[] = __('None of the jobs could be created. The request has been cancelled.', true);
			return false;
		}

		if ($jobErrors) {
			$errors[] = __('Not all jobs could be created.', true);
		}

		if (!$this->defer('scan')) {
			$errors[] = __('Failed to queue the request for the virus scanner. Please contact system administration.', true);
		}

		return true;
	}

	/**
	 * Cancel the request
	 *
	 * @return void
	 */
	public function cancel()
	{
		if (!$this->id) {
			return;
		}

		$this->save(array('Request' => array(
			'state' => self::STATE_CANCELLED,
			'expire' => date('Y-m-d H:i:s', time() - 1)
		)));
	}

	/**
	 * Set Expired state on all expired requests
	 *
	 * @return boolean Success
	 */
	public function expireAll()
	{
		return $this->updateAll(
			array('Request.state' => self::STATE_EXPIRED),
			array(
				'Request.expire <=' => date('Y-m-d H:i:s'),
				'Request.state' => array(self::STATE_UPLOADING, self::STATE_SCAN_QUEUED, self::STATE_QUEUED)
			)
		);
	}

	/**
	 * Generate a zipfile containing the original request and all results
	 *
	 * @return array an array containing the directory and filename of the zipfile or null
	 */
	public function createZip()
	{
		if (!isset($this->id)) {
			return null;
		}

		$directory = $this->Behaviors->File->settings['directory'];
		$filename = $this->field('filename');
		$zipdir = 'officeshots-' . substr($filename, 0, strrpos($filename, '.'));
		$filename = $zipdir . '.zip';

		if (file_exists($directory . DS . $filename)) {
			unlink($directory . DS . $filename);
		}
		$zip = new ZipArchive();
		$zip->open($directory . DS . $filename, ZIPARCHIVE::CREATE);
		$zip->addFromString($zipdir . '/' . $this->field('filename'), $this->getContents());

		$jobs = $this->Job->find('all', array(
			'conditions' => array(
				'Job.request_id' => $this->id,
				'Job.result_id <>' => '',
			)
		));

		foreach ($jobs as $job) {
			$appname = $job['Application']['name'] . '_'
			         . $job['Job']['version']
				 . '_' . $job['Platform']['name'];
			$appname = preg_replace('/[^a-z0-9._-]/', '_', low($appname));
			$appname = preg_replace('/_{2,}/', '_', $appname);

			$this->Job->Result->id = $job['Result']['id'];
			$extension = substr($job['Result']['filename'], strrpos($job['Result']['filename'], '.') + 1);
			$zip->addFromString($zipdir . '/' . $appname . '.' . $extension, $this->Job->Result->getContents());
		}
		$zip->close();

		return compact('directory', 'filename');
	}

	/**
	 * Scan the request for viruses and update the scan status
	 *
	 * @return None
	 */
	public function scan()
	{
		$this->read();

		// Check for the correct state. In case of queue errors a scan could be scheduled multiple times
		if ($this->data['Request']['state'] != self::STATE_SCAN_QUEUED) {
			return;
		}

		$clamd = new Clamd();
		$path = $this->getPath();
		$result = '';

		if (!$path) {
			$this->log('Request could not be scanned. ' . $path . ' (Request ID: ' . $this->id . ') does not exists. ');
			return;
		}

		$status = $clamd->scan($path, $result);
		if ($status == Clamd::OK) {
			// Scan OK. Queue the Request for processing
			$this->data['Request']['state'] = self::STATE_QUEUED;
			$this->log('Clamd scanned ' . $path . ' (Request ID: ' . $this->id . '): OK', LOG_DEBUG);
		} elseif ( $status == Clamd::FOUND ) {
			// There was a virus. Remove all jobs and alert user
			// Note that the file is *not* deleted. This was we can later check if there really was a virus
			$this->Job->deleteAll(array('Job.request_id' => $this->id));
			$this->data['Request']['state'] = self::STATE_SCAN_FOUND;
			$this->data['Request']['state_info'] = $result;
			$this->data['Request']['expire'] = date('Y-m-d H:i:s');
			$this->log('Clamd scanned ' . $path . ' (Request ID: ' . $this->id . '): FOUND ' . $result, LOG_DEBUG);
		} else {
			// There was an error. Remove all jobs and alert user
			if ($status === false) {
				$result = $clamd->lastError();
				$status = Clamd::ERROR;
			}

			$this->Job->deleteAll(array('Job.request_id' => $this->id));
			$this->data['Request']['state'] = self::STATE_SCAN_FAILED;
			$this->data['Request']['state_info'] = $result;
			$this->data['Request']['expire'] = date('Y-m-d H:i:s');

			$this->log('Clamd error scanning ' . $path . ' (Request ID: ' . $this->id . '): ' . $result);
		}

		$this->save();
	}
}

?>
