<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Results controller
 */
class ResultsController extends AppController
{
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form');
	
	/** @var array The components this controller uses */
	public $components = array('AuthCert');

	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		if (Configure::read('Auth.allowAnonymous')) {
			$this->AuthCert->allow('view', 'download');
		}
	}

	/**
	 * Get the result by it's ID and check access control.
	 *
	 * Calling methods should ensure that Job.Request is contained in the model query.
	 *
	 * @param string $id The result ID
	 * @param return array An array containing the result.
	 */
	private function _getResult($id)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Result.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'add'));
		}
		
		$result = $this->Result->read(null, $id);
		if ($result['Job']['Request']['user_id'] != $this->AuthCert->user('id')) {
			$this->Session->setFlash(__('Invalid Result.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'add'));
		}

		return $result;
	}
	
	/**
	 * You can't index results. Redirect to requests.
	 * @return void
	 */
	public function index()
	{
		$this->redirect(array('controller' => 'requests', 'action' => 'index'));
	}

	/**
	 * View a single Result
	 *
	 * @param string $id The Result ID
	 * @return void
	 */
	public function view($id = null)
	{
		$this->helpers[] = 'ResultModel';

		$this->Result->contain(array(
			'Job',
			'Job.Request',
			'Job.Application',
			'Job.Platform',
			'Factory',
			'Factory.User',
			'Factory.Operatingsystem',
			'Format',
			'Mimetype',
		));

		$result = $this->_getResult($id);
		$this->set(array(
			'result' => $result,
			'canDelete' => $this->__permitted('results', 'delete'),
		));
	}

	/**
	 * Action to download a single result
	 *
	 * @param string $id The Result ID
	 * @return void
	 */
	public function download($id = null)
	{
		$this->Result->contain(array(
			'Job',
			'Job.Request',
			'Mimetype',
		));

		$result = $this->_getResult($id);
		$extpos = strrpos($result['Result']['filename'], '.');
		$filename = substr($result['Result']['filename'], 0, $extpos);
		$extension = substr($result['Result']['filename'], $extpos + 1);
		$directory = $this->Result->Behaviors->File->settings['directory'];

		$this->view = 'Media';
		$this->set(array(
			'id' => $result['Result']['path'],
			'name' => $filename,
			'download' => true,
			'extension' => $extension,
			'path' => $directory . DS,
			// NOTE: This short-circuits CakePHP's crude filetype check
			'mimeType' => array($extension => $result['Mimetype']['name']),
		));
	}

	/**
	 * Delete a single result.
	 *
	 * This is only for backend developers who want to re-use their requests while developing
	 *
	 * @param string $id The Result ID
	 * @return void
	 */
	public function delete($id = null)
	{
		$this->Result->contain(array(
			'Job',
			'Job.Request',
		));

		$result = $this->_getResult($id);
		if ($this->Result->del($id)) {
			$this->Session->setFlash(__('Result deleted', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'view', $result['Job']['Request']['id']));
		}
	}


	public function admin_index()
	{
		$this->Result->recursive = 0;
		$this->set('results', $this->paginate());
	}

	public function admin_view($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Result.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('result', $this->Result->read(null, $id));
	}

	public function admin_add()
	{
		if (!empty($this->data)) {
			$this->Result->create();
			if ($this->Result->save($this->data)) {
				$this->Session->setFlash(__('The Result has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Result could not be saved. Please, try again.', true));
			}
		}
		$factories = $this->Result->Factory->find('list');
		$formats = $this->Result->Format->find('list');
		$this->set(compact('factories', 'formats'));
	}

	public function admin_edit($id = null)
	{
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Result', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Result->save($this->data)) {
				$this->Session->setFlash(__('The Result has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Result could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Result->read(null, $id);
		}
		$factories = $this->Result->Factory->find('list');
		$formats = $this->Result->Format->find('list');
		$this->set(compact('factories','formats'));
	}

	public function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Result', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Result->del($id)) {
			$this->Session->setFlash(__('Result deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}

?>
