<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Requests controller
 */
class RequestsController extends AppController
{
	/** @var array The components this controller uses */
	public $components = array('AuthCert', 'RequestHandler');
	
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form', 'Javascript', 'RequestModel');

	/** @var array Add Request and Worker model */
	public $uses = array('Request', 'Worker', 'Mimetype', 'User');

	/** @var array Set default sort order for paginate */
	public $paginate = array(
		'order' => array('Request.created' => 'desc')
	);

	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->AuthCert->allow('add');

		if (Configure::read('Auth.allowAnonymous')) {
			$this->AuthCert->allow('view', 'download');
		}
	}

	/**
	 * List all your requests
	 * @return void
	 */
	public function index()
	{
		$this->set('requests', $this->paginate('Request', array('Request.user_id' => $this->AuthCert->user('id'))));
	}

	/**
	 * Get a Request by the request ID and check access control
	 *
	 * @param string $id The request ID
	 * @return array An array containing the request
	 */
	private function _getRequest($id)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Request.', true));
			$this->redirect(array('action'=>'add'));
		}
		
		$this->Request->contain(array(
			'Format',
			'Mimetype',
			'Mimetype.Doctype',
			'Job',
			'Job.Platform',
			'Job.Application',
			'Job.Result',
			'Job.Result.Mimetype',
			'Job.Result.Format',
		));

		$request = $this->Request->read(null, $id);
		if ($request['Request']['user_id'] != $this->AuthCert->user('id')) {
			$this->Session->setFlash(__('Invalid Request.', true));
			$this->redirect(array('action'=>'add'));
		}

		return $request;
	}

	/**
	 * View a single request
	 *
	 * @param string $id The request ID
	 * @return void
	 */
	public function view($id = null)
	{
		// Used to display the jobs associated with the Request
		$this->helpers[] = 'JobModel';

		$request = $this->_getRequest($id);
		$this->set(array(
			'request' => $request,
			'canDeleteResults' => $this->__permitted('results', 'delete'),
		));
	}

	/**
	 * Add a new request
	 * @return void
	 */
	public function add()
	{
		if (!empty($this->data)) {
			// Access control
			if (!Configure::read('Auth.allowAnonymous') && !$this->AuthCert->user()) {
				$this->Session->setFlash(__('You are not allowed to submit requests anonymously.', true));
				$this->redirect(array('action'=>'add'));
			}

			// Check the daily request limits
			if ($user = $this->AuthCert->user()) {
				$requestLimit = Configure::read('Request.limitRegistered');

				$user = $this->User->find(array('User.id' => $user['User']['id']));
				$limits = Set::extract('/Group/request_limit', $user);
				$limits[] = $requestLimit;
				$requestLimit = max($limits);

				$numRequests = $this->Request->find('count', array(
					'conditions' => array(
						'Request.user_id' => $user['User']['id'],
						'Request.created >' => date('Y-m-d H:i:s', strtotime('-1 day'))
					)
				));

				if ($numRequests > $requestLimit) {
					$this->Session->setFlash(sprintf(__('You have exceeded your quota of %d daily requests.', true), $requestLimit));
					$this->redirect(array('action'=>'add'));
				}
			} else {
				$requestLimit = Configure::read('Request.limitAnonymous');
				$numRequests = $this->Request->find('count', array(
					'conditions' => array(
						'Request.ip_address' => inet_ptod($this->RequestHandler->getClientIP()),
						'Request.created >' => date('Y-m-d H:i:s', strtotime('-1 day'))
					)
				));

				if ($numRequests > $requestLimit) {
					$this->Session->setFlash(sprintf(__('You have exceeded your quota of %d daily requests.', true), $requestLimit));
					$this->redirect(array('action'=>'add'));
				}
			}

			// Create a new request
			$this->Request->create();
			$this->data['Request']['user_id'] = (string) $this->AuthCert->user('id');
			$this->data['Request']['ip_address'] = inet_ptod($this->RequestHandler->getClientIP());

			// Add the jobs to the request
			foreach ($this->data['Request']['App'] as $app) {
				list($platform_id, $doctype_code, $application_id, $version) = explode('_', $app);
				$this->data['Request']['Job'][] = compact('platform_id', 'doctype_code', 'application_id', 'version');
			}

			// Save the request
			$errors = array();
			if (!$this->Request->add($this->data, $errors)) {
				$this->Session->setFlash(implode("<br />\n", $errors));
				$this->redirect(array('action'=>'add'));
			}

			if (sizeof($errors)) {
				$this->Session->setFlash(implode("<br />\n", $errors));
			}

			$this->redirect(array('action' => 'view', 'id' => $this->Request->id));
		}

		$this->set('can_have_factories', $this->__permitted('factories', 'edit'));
		$this->set('can_submit_requests', ($this->__permitted('factories', 'edit') || $this->AuthCert->user()));
		
		// TODO: The result of all the below actions should really be cached for better performance
		$workers = $this->Worker->getActive();

		// Give all the active workers a unique, short ID.
		// We need this for checking and unchecking sets of applications
		$count = 0;
		foreach ($workers as &$worker) {
			$worker['short_id'] = 'c'.$count++;
		}
		unset($worker); // Don't leave it assigned or bugs will occur below

		// Get all the other information we need to build the front page
		$platforms = $this->Worker->Factory->Operatingsystem->Platform->find('all');
		$doctypes = $this->Worker->Application->Doctype->find('all');
		$formats = $this->Request->Format->find('list');
		// $formatmap = $this->Request->Format->find('list', array('fields' => array('Format.id', 'Format.code')));
		$mimetypes = $this->Mimetype->find('all');

		// Build a format map which consists of sets of active applications
		$sets = array(
			'platform'    => array(),
			'extension'   => array(),
			'format'      => array(),
			'latest'      => array(),
			'development' => Set::extract('/Worker[development=1]/../short_id', $workers),
			'all'         => Set::extract('/short_id', $workers)
		);

		foreach ($platforms as $platform) {
			$sets['platform'][$platform['Platform']['id']] = Set::extract('/Platform[id=' . $platform['Platform']['id'] . ']/../short_id', $workers);
		}
		foreach ($mimetypes as $mimetype) {
			if ($mimetype['Mimetype']['doctype_id']) {
				$sets['extension'][$mimetype['Mimetype']['extension']] = Set::extract('/Doctype[id=' . $mimetype['Mimetype']['doctype_id'] . ']/../short_id', $workers);
			}
		}
		foreach ($formats as $format_id => $format) {
			$sets['format'][$format_id] = Set::extract('/Format[id=' . $format_id . ']/../short_id', $workers);
		}

		// This is the tricky bit. Set the highest version for every application/platform combo as default
		// This requires the workers array to be sorted my platform, app, version
		$short_id = false;
		$platform_id = '';
		$application_id = '';
		foreach ($workers as $worker) {
			if ($worker['Worker']['development']) {
				continue;
			}

			if ($worker['Application']['id'] != $application_id || $worker['Platform']['id'] != $platform_id) {
				if ($short_id) {
					$sets['latest'][] = $short_id;
				}

				$platform_id = $worker['Platform']['id'];
				$application_id = $worker['Application']['id'];
			}

			$short_id = $worker['short_id'];
		}
		$sets['latest'][] = $short_id; // don't forget to add the last one.

		$this->set(compact('workers', 'platforms', 'doctypes', 'formats', 'sets', 'mimetypes'));
	}

	/**
	 * Download a request
	 *
	 * @param string $id The request ID
	 */
	public function download($id = null)
	{
		$request = $this->_getRequest($id);
		extract($this->Request->createZip());
		
		$this->view = 'Media';
		$this->set(array(
			'id' => $filename,
			'name' => substr($filename, 0, strrpos($filename, '.')),
			'download' => true,
			'extension' => 'zip',
			'path' => $directory . DS,
		));
	}

	/**
	 * Extend the deadline of a request
	 *
	 * @param string $id The request ID
	 */
	public function extend($id = null)
	{
		$request = $this->_getRequest($id);

		if (strtotime($request['Request']['expire']) < time()) {
			$this->Session->setFlash(__('You cannot extend a request that has expired.', true));
			$this->redirect(array('action' => 'view', $id));
		}

		$this->Request->saveField('expire', date('Y-m-d H:i:s', time() + Configure::read('Request.expire')));
		$this->redirect(array('action' => 'view', $id));
	}

	/**
	 * Cancel a job by setting it's expiration time
	 *
	 * @param string $id The request ID
	 */
	public function cancel($id = null)
	{
		$request = $this->_getRequest($id);
		$this->Request->cancel();
		$this->redirect(array('action' => 'view', $id));
	}

	/**
	 * List all requests
	 * @return void
	 */
	public function admin_index()
	{
		$this->Request->recursive = 0;
		$this->set('requests', $this->paginate());
	}

	/**
	 * View a single request
	 *
	 * @param string $id The request ID
	 * @return void
	 */
	public function admin_view($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Request.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('request', $this->Request->read(null, $id));
	}

	/**
	 * Add a new request manually
	 * @return void
	 */
	public function admin_add()
	{
		if (!empty($this->data)) {
			$this->data['Request']['ip_address'] = inet_ptod($this->data['Request']['ip_address']);
			$this->Request->create();
			if ($this->Request->save($this->data)) {
				$this->Session->setFlash(__('The Request has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$flash = $this->Request->Behaviors->File->errors;
				array_unshift($flash, __('The Request could not be saved. Please, try again.', true));
				$this->Session->setFlash(implode('<br />', $flash));
			}
		}
		$users = $this->Request->User->find('list');
		$mimetypes = $this->Request->Mimetype->find('list');
		$formats = $this->Request->Format->find('list');
		$this->set(compact('users','mimetypes','formats'));
	}

	/**
	 * Edit a request
	 *
	 * @param string $id The request ID
	 * @return void
	 */
	public function admin_edit($id = null)
	{
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Request', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			$this->data['Request']['ip_address'] = inet_ptod($this->data['Request']['ip_address']);
			if ($this->Request->save($this->data)) {
				$this->Session->setFlash(__('The Request has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$flash = $this->Request->Behaviors->File->errors;
				array_unshift($flash, __('The Request could not be saved. Please, try again.', true));
				$this->Session->setFlash(implode('<br />', $flash));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Request->read(null, $id);
			$this->data['Request']['ip_address'] = inet_dtop($this->data['Request']['ip_address']);
		}
		$users = $this->Request->User->find('list');
		$mimetypes = $this->Request->Mimetype->find('list');
		$formats = $this->Request->Format->find('list');
		$this->set(compact('users','mimetypes','formats'));
	}

	/**
	 * Delete a request
	 *
	 * @param string $id The request ID
	 * @return void
	 */
	public function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Request', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Request->del($id)) {
			$this->Session->setFlash(__('Request deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}

?>
