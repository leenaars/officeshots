<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class SuitesShell extends Shell
{
	/** @var array The models to use */
	public $uses = array('Testsuite');

	/**
	 * Override startup() so no welcome message is printed
	 */
	public function startup() {}

	/**
	 * Main function. Print help and exit.
	 */
	public 	function main()
	{
		$this->help();
	}

	/**
	 * Update the test suites
	 */
	public function sync()
	{
		$suite_id = $this->_getSuite();
		$this->Testsuite->synchronise($suite_id);
	}

	/**
	 * Update the test suites
	 */
	public function addJobs()
	{
		$this->Testsuite->addJobs();
	}

	/**
	 * Ask for a test suite
	 */
	private function _getSuite()
	{
		$options = array(1 => 'All test suites');
		$suites = $this->Testsuite->find('list', array(
			'order' => 'Testsuite.name ASC',
			'recursive' => -1,
		));

		foreach ($suites as $suite) {
			$options[] = $suite;
		}

		$this->out('Please coose a test suite.');
		$this->hr();
		foreach ($options as $index => $option) {
			$this->out(sprintf('[%d] %s', $index, $option));
		}

		$suite_id = null;
		while ($suite_id === null) {
			$suiteNum = $this->in('Enter a test suite number, or q to quit', null, 'q');

			if (strtolower($suiteNum) === 'q') {
				$this->out('Exit');
				$this->_stop();
			}

			$suiteNum = intval($suiteNum) - 2;
			if ($suiteNum >= count($suites)) {
				$this->out('Invalid selection');
				continue;
			}

			if ($suiteNum == -1) {
				$suite_id = false;
			} else {
				$suite_ids = array_keys($suites);
				$suite_id = $suite_ids[$suiteNum];
			}
		}

		return $suite_id;
	}

	/**
	 * Print shell help
	 */
	public function help()
	{
		$this->out('Commandline interface to the Officeshots test suites');
		$this->hr();
		$this->out("Usage: cake suites <command>");
		$this->hr();
		$this->out('Commands:');
		$this->out("\n\tsync\n\t\tSynchronise all ODF testsuites.");
		$this->out("\n\thelp\n\t\tShow this help");
		$this->out('');
	}
}

?>
