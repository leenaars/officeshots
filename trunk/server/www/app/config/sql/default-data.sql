-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 04, 2010 at 01:10 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6-1+lenny6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `officeshots`
--

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`id`, `name`, `created`, `modified`, `icon`) VALUES
('496dd376-376c-4a8d-be75-5492c0a80105', 'AbiWord', '2009-01-14 12:58:46', '2010-03-22 12:48:29', 'abiword.png'),
('4aeece2a-5ffc-4fc9-8ae1-5ea5c0a80105', 'KOffice', '2009-11-02 13:18:50', '2010-03-22 12:57:17', 'koffice.png'),
('4974af01-4c9c-40ab-ac41-797fc0a80105', 'OpenOffice.org', '2009-01-19 17:49:05', '2010-03-31 00:34:17', 'openoffice.png'),
('4974af37-0018-4f89-9505-7973c0a80105', 'Gnumeric', '2009-01-19 17:49:59', '2010-03-22 12:56:11', 'gnumeric.png'),
('498feee4-ac00-4834-8054-2918c0a80105', 'Office Reader', '2009-02-09 09:52:52', '2009-02-15 14:47:51', ''),
('49afa355-d204-4db9-a885-094ac0a80105', 'RedOffice', '2009-03-05 11:03:01', '2010-03-23 11:30:32', 'redoffice.png'),
('49e47b77-f3ac-4cd2-8047-3d8ec0a80105', 'Go-OO', '2009-04-14 14:03:03', '2010-03-22 12:55:29', 'go-oo.png'),
('4acf232d-78a4-4fb0-8631-1e71c0a80105', 'Corel WordPerfect Office', '2009-10-09 13:49:01', '2010-03-22 12:52:53', 'wordperfect.png'),
('4aeecc92-b838-4b38-bcc8-445bc0a80105', 'Ability Office', '2009-11-02 13:12:02', '2010-03-22 12:47:57', 'ability-office.png'),
('4aeeccb5-d11c-4b51-8c79-445bc0a80105', 'Acrobat.com', '2009-11-02 13:12:37', '2010-03-22 12:48:49', 'acrobat.png'),
('4aeeccc1-3c8c-48af-81fc-445bc0a80105', 'Aspose.Net', '2009-11-02 13:12:49', '2010-03-22 12:49:07', 'aspose.png'),
('4aeeccf5-e91c-4778-b613-5fc2c0a80105', 'Celframe Office', '2009-11-02 13:13:41', '2010-03-22 12:52:07', 'celframe-office.png'),
('4aeecd38-1960-408b-806c-1816c0a80105', 'DigiDNA FileApp', '2009-11-02 13:14:48', '2010-03-22 12:53:32', 'fileapp-pro.png'),
('4aeecd50-d890-40e9-95b5-632dc0a80105', 'EditGrid', '2009-11-02 13:15:12', '2009-11-02 13:15:12', ''),
('4aeecd62-d4bc-4e62-a387-632dc0a80105', 'EIOffice', '2009-11-02 13:15:30', '2010-03-22 12:53:51', ''),
('4aeecd86-ce6c-41f6-ac96-632dc0a80105', 'EuroOffice', '2009-11-02 13:16:06', '2010-03-22 12:54:34', ''),
('4aeecdc9-dd94-44c4-aba3-632dc0a80105', 'Feng Office', '2009-11-02 13:17:13', '2009-11-02 13:17:13', ''),
('4aeecdd6-7a50-4a65-b6e7-632dc0a80105', 'GOffice', '2009-11-02 13:17:26', '2009-11-02 13:24:55', ''),
('4aeecdef-5cb0-471e-9651-5ea5c0a80105', 'Google Docs', '2009-11-02 13:17:51', '2010-03-22 12:56:29', 'google-docs.png'),
('4aeece56-beec-4e38-a948-5ea5c0a80105', 'Lotus Symphony', '2009-11-02 13:19:34', '2010-03-22 12:57:59', 'symphony.png'),
('4aeece72-4024-4ed5-b741-5e82c0a80105', 'Microsoft Office', '2009-11-02 13:20:02', '2010-03-22 12:58:19', 'ms-office.png'),
('4aeece80-a878-4e7e-b11d-5e82c0a80105', 'Microsoft Office (ClearAge plugin)', '2009-11-02 13:20:16', '2010-03-22 12:58:39', 'ms-office.png'),
('4aeece8a-91d4-46f3-bf14-5e82c0a80105', 'Microsoft Office (Sun plugin)', '2009-11-02 13:20:26', '2010-03-22 12:59:02', 'ms-office.png'),
('4aeecf33-1c10-4e0a-b734-6a72c0a80105', 'NeoOffice', '2009-11-02 13:23:15', '2010-03-22 13:01:50', ''),
('4aeecf5e-0524-4931-9a3d-6a72c0a80105', 'OpenGoo', '2009-11-02 13:23:58', '2009-11-02 13:23:58', ''),
('4aeecfb7-f238-497c-b0bb-5c62c0a80105', 'StarOffice', '2009-11-02 13:25:27', '2010-03-22 13:08:38', 'staroffice.png'),
('4aeecff0-350c-4047-ae6e-5ea5c0a80105', 'Zoho', '2009-11-02 13:26:24', '2009-11-02 13:26:24', ''),
('4aeecff8-3654-4bb4-9bce-5ea5c0a80105', 'ZCubes', '2009-11-02 13:26:32', '2009-11-02 13:26:32', ''),
('4aeed02b-09c8-46a8-87da-6a72c0a80105', 'SoftMaker Office', '2009-11-02 13:27:23', '2010-03-22 13:05:19', 'softmaker-office.png'),
('4aeed040-0128-412a-9aba-6a72c0a80105', 'TextMaker Viewer', '2009-11-02 13:27:44', '2009-11-02 13:27:44', ''),
('4aeed07b-b608-437f-a237-632dc0a80105', 'Visor ODF', '2009-11-02 13:28:43', '2009-11-02 13:28:43', ''),
('4b97ca5d-e1d8-4c71-8954-1ebdc1acb699', 'Atlantis', '2010-03-10 17:35:41', '2010-03-10 17:35:41', '');

--
-- Dumping data for table `applications_doctypes`
--

INSERT INTO `applications_doctypes` (`id`, `application_id`, `doctype_id`) VALUES
('4ba7590d-fc84-4f25-85b3-71a3c1acb699', '496dd376-376c-4a8d-be75-5492c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75adb-86b8-46c5-a3e5-725ac1acb699', '4974af37-0018-4f89-9505-7973c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75b47-23c4-4061-b857-729ac1acb699', '4aeece56-beec-4e38-a948-5ea5c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75b5b-c78c-4456-b07d-7237c1acb699', '4aeece72-4024-4ed5-b741-5e82c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75b1d-9ce0-4d42-b137-7238c1acb699', '4aeece2a-5ffc-4fc9-8ae1-5ea5c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75b5b-975c-4e1b-90d2-7237c1acb699', '4aeece72-4024-4ed5-b741-5e82c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4bb27c69-60bc-4a24-af7b-67f7c1acb699', '4974af01-4c9c-40ab-ac41-797fc0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75cff-cc74-48c5-bc27-730ec1acb699', '4aeed02b-09c8-46a8-87da-6a72c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75cff-03ac-4977-a91f-730ec1acb699', '4aeed02b-09c8-46a8-87da-6a72c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('49981d07-54cc-4498-8e51-1fcbc0a80105', '498feee4-ac00-4834-8054-2918c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('49981d07-0bc0-4a78-a8fb-1fcbc0a80105', '498feee4-ac00-4834-8054-2918c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('49981d07-bc74-48bb-bb09-1fcbc0a80105', '498feee4-ac00-4834-8054-2918c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba89848-9d18-40c4-b8df-0501c1acb699', '49afa355-d204-4db9-a885-094ac0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75ab1-38b4-487b-bf75-71a1c1acb699', '49e47b77-f3ac-4cd2-8047-3d8ec0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75aed-4b98-4a38-bf8f-71a1c1acb699', '4aeecdef-5cb0-471e-9651-5ea5c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75aed-8078-4cbd-b5d6-71a1c1acb699', '4aeecdef-5cb0-471e-9651-5ea5c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75a15-9f00-44c2-8cb9-7076c1acb699', '4acf232d-78a4-4fb0-8631-1e71c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75a4f-6678-44f4-8db6-7074c1acb699', '4aeecd62-d4bc-4e62-a387-632dc0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75a4f-2e78-4234-ac0a-7074c1acb699', '4aeecd62-d4bc-4e62-a387-632dc0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba758ed-3574-4561-970b-71a3c1acb699', '4aeecc92-b838-4b38-bcc8-445bc0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75921-afa0-4083-abfa-71a3c1acb699', '4aeeccb5-d11c-4b51-8c79-445bc0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75921-e5ac-4e4a-abcb-71a3c1acb699', '4aeeccb5-d11c-4b51-8c79-445bc0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75921-fae8-45ab-877b-71a3c1acb699', '4aeeccb5-d11c-4b51-8c79-445bc0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75933-fbd0-4d82-a502-71a3c1acb699', '4aeeccc1-3c8c-48af-81fc-445bc0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba759e7-11b8-4be0-bcb0-71a1c1acb699', '4aeeccf5-e91c-4778-b613-5fc2c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba759e7-07d0-4017-960f-71a1c1acb699', '4aeeccf5-e91c-4778-b613-5fc2c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba759e7-d548-4aa5-a77c-71a1c1acb699', '4aeeccf5-e91c-4778-b613-5fc2c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75a15-4c4c-445c-be6f-7076c1acb699', '4acf232d-78a4-4fb0-8631-1e71c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75a15-8258-4fb6-89b7-7076c1acb699', '4acf232d-78a4-4fb0-8631-1e71c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75a3c-470c-41ae-ad23-7074c1acb699', '4aeecd38-1960-408b-806c-1816c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75a3c-7b88-492d-b758-7074c1acb699', '4aeecd38-1960-408b-806c-1816c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75a3c-92b8-48ce-8ae3-7074c1acb699', '4aeecd38-1960-408b-806c-1816c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4aeecd50-a6c8-44ff-8225-632dc0a80105', '4aeecd50-d890-40e9-95b5-632dc0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75a4f-8834-419d-a5ee-7074c1acb699', '4aeecd62-d4bc-4e62-a387-632dc0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75a7a-6d18-472f-beb4-7076c1acb699', '4aeecd86-ce6c-41f6-ac96-632dc0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75a7a-a450-4963-bde8-7076c1acb699', '4aeecd86-ce6c-41f6-ac96-632dc0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75a7a-bfcc-4d21-b6ff-7076c1acb699', '4aeecd86-ce6c-41f6-ac96-632dc0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75ab1-f21c-45ab-90a4-71a1c1acb699', '49e47b77-f3ac-4cd2-8047-3d8ec0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75ab1-2440-4bb6-8a6a-71a1c1acb699', '49e47b77-f3ac-4cd2-8047-3d8ec0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4aeecdc9-a974-4df5-849c-632dc0a80105', '4aeecdc9-dd94-44c4-aba3-632dc0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4aeecdc9-f5a0-41d6-9c8f-632dc0a80105', '4aeecdc9-dd94-44c4-aba3-632dc0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4aeecdc9-3b8c-4200-b728-632dc0a80105', '4aeecdc9-dd94-44c4-aba3-632dc0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4aeecf97-ecbc-4a16-945c-5ee6c0a80105', '4aeecdd6-7a50-4a65-b6e7-632dc0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75aed-9e4c-4d42-98df-71a1c1acb699', '4aeecdef-5cb0-471e-9651-5ea5c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75b1d-4964-4fb0-9058-7238c1acb699', '4aeece2a-5ffc-4fc9-8ae1-5ea5c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75b1d-7f70-479f-a455-7238c1acb699', '4aeece2a-5ffc-4fc9-8ae1-5ea5c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75b47-596c-4ce4-83c1-729ac1acb699', '4aeece56-beec-4e38-a948-5ea5c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75b47-6930-400e-9206-729ac1acb699', '4aeece56-beec-4e38-a948-5ea5c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75b5b-de58-4d9d-82c7-7237c1acb699', '4aeece72-4024-4ed5-b741-5e82c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75b6f-9464-4a91-a147-7237c1acb699', '4aeece80-a878-4e7e-b11d-5e82c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75b87-9a20-484c-8ffc-7238c1acb699', '4aeece8a-91d4-46f3-bf14-5e82c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75c2e-cf1c-4c0c-a364-71a2c1acb699', '4aeecf33-1c10-4e0a-b734-6a72c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75c2e-058c-4934-bd5d-71a2c1acb699', '4aeecf33-1c10-4e0a-b734-6a72c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75c2e-1de8-4db5-9a4a-71a2c1acb699', '4aeecf33-1c10-4e0a-b734-6a72c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4bb27c69-9470-4926-b49d-67f7c1acb699', '4974af01-4c9c-40ab-ac41-797fc0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4bb27c69-ad30-49c9-924d-67f7c1acb699', '4974af01-4c9c-40ab-ac41-797fc0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4aeecf5e-d4ec-46e0-bccb-6a72c0a80105', '4aeecf5e-0524-4931-9a3d-6a72c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75dc6-2df8-4bb7-b60d-729ac1acb699', '4aeecfb7-f238-497c-b0bb-5c62c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba89848-7fa8-4299-b9ae-0501c1acb699', '49afa355-d204-4db9-a885-094ac0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba89848-4938-4560-9aa0-0501c1acb699', '49afa355-d204-4db9-a885-094ac0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4aeecff0-0efc-483a-be6b-5ea5c0a80105', '4aeecff0-350c-4047-ae6e-5ea5c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4aeecff0-5b28-4a8f-9c6a-5ea5c0a80105', '4aeecff0-350c-4047-ae6e-5ea5c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4aeecff0-9fe8-4778-9179-5ea5c0a80105', '4aeecff0-350c-4047-ae6e-5ea5c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4aeecff8-fa64-4a3c-a607-5ea5c0a80105', '4aeecff8-3654-4bb4-9bce-5ea5c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4aeecff8-4564-4c19-a4ff-5ea5c0a80105', '4aeecff8-3654-4bb4-9bce-5ea5c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4aeecff8-8894-4320-ad81-5ea5c0a80105', '4aeecff8-3654-4bb4-9bce-5ea5c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75dc6-0fc0-4a74-afc7-729ac1acb699', '4aeecfb7-f238-497c-b0bb-5c62c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75cff-194c-4142-9d07-730ec1acb699', '4aeed02b-09c8-46a8-87da-6a72c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba75dc6-d694-4625-b994-729ac1acb699', '4aeecfb7-f238-497c-b0bb-5c62c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4aeed040-d280-45a1-8632-6a72c0a80105', '4aeed040-0128-412a-9aba-6a72c0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4aeed07b-7d38-47ad-b262-632dc0a80105', '4aeed07b-b608-437f-a237-632dc0a80105', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4aeed07b-c450-4988-ad3a-632dc0a80105', '4aeed07b-b608-437f-a237-632dc0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4aeed07b-0528-4e87-b33f-632dc0a80105', '4aeed07b-b608-437f-a237-632dc0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4b97ca5d-aabc-42d4-aa10-1ebdc1acb699', '4b97ca5d-e1d8-4c71-8954-1ebdc1acb699', '4970527f-d0a0-4206-b718-7900c0a80105'),
('4ba758ed-2614-472e-bb3e-71a3c1acb699', '4aeecc92-b838-4b38-bcc8-445bc0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba758ed-ef40-4a1f-a600-71a3c1acb699', '4aeecc92-b838-4b38-bcc8-445bc0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75933-dc6c-426c-a90b-71a3c1acb699', '4aeeccc1-3c8c-48af-81fc-445bc0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75933-a660-4697-8d8c-71a3c1acb699', '4aeeccc1-3c8c-48af-81fc-445bc0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75b6f-794c-4793-9150-7237c1acb699', '4aeece80-a878-4e7e-b11d-5e82c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75b6f-4278-4810-a916-7237c1acb699', '4aeece80-a878-4e7e-b11d-5e82c0a80105', '497052a9-0090-4c06-a483-7900c0a80105'),
('4ba75b87-7f08-4691-9e70-7238c1acb699', '4aeece8a-91d4-46f3-bf14-5e82c0a80105', '49705294-bfa8-4ee2-9cec-7900c0a80105'),
('4ba75b87-48fc-47bb-b2f7-7238c1acb699', '4aeece8a-91d4-46f3-bf14-5e82c0a80105', '497052a9-0090-4c06-a483-7900c0a80105');

--
-- Dumping data for table `doctypes`
--

INSERT INTO `doctypes` (`id`, `name`, `code`, `order`) VALUES
('4970527f-d0a0-4206-b718-7900c0a80105', 'OpenDocument Text (odt, ott)', 'odt', 1),
('49705294-bfa8-4ee2-9cec-7900c0a80105', 'OpenDocument Spreadsheet (ods, ots)', 'ods', 2),
('497052a9-0090-4c06-a483-7900c0a80105', 'OpenDocument Presentation (odp, otp)', 'odp', 3);

--
-- Dumping data for table `formats`
--

INSERT INTO `formats` (`id`, `name`, `code`, `icon`) VALUES
('496de908-d92c-4e3b-9447-558fc0a80105', 'PDF Export', 'pdf', 'pdf-format.png'),
('496de919-96f4-4c15-8b14-55e6c0a80105', 'Screenshot', 'png', 'image-format.png'),
('496de976-9a10-403c-aa90-55e6c0a80105', 'Round-trip ODF', 'odf', 'roundtrip-format.png');

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `request_limit`, `default_memberlist`, `created`, `modified`, `default`) VALUES
('4964c4fc-e7c8-4b21-aef2-6197c0a80105', 'System Developers', 100, '', '2009-01-07 16:06:36', '2009-07-21 17:00:13', 0),
('49b10879-6cf8-410e-bbe6-1c6ac0a80105', 'Factory Owners', 50, '', '2009-03-06 12:26:49', '2009-07-21 17:00:21', 0),
('4afbcfa5-d7c4-4a14-898f-5c7fc1acb699', 'All Users', 50, '', '2009-11-12 10:04:37', '2009-11-12 10:04:37', 1),
('4bb25987-1248-45f1-8ffd-65d2c1acb699', 'Testsuite editors', 50, '', '2010-03-30 22:05:27', '2010-03-30 22:05:27', 0);

--
-- Dumping data for table `groups_users`
--

INSERT INTO `groups_users` (`id`, `group_id`, `user_id`) VALUES
('4be00052-36c4-47d7-b9f4-5b9bc1acb699', '4964c4fc-e7c8-4b21-aef2-6197c0a80105', '4be00052-cbe0-4adb-ac41-5b9bc1acb699');

--
-- Dumping data for table `mimetypes`
--

INSERT INTO `mimetypes` (`id`, `name`, `icon`, `doctype_id`, `format_id`, `extension`) VALUES
('49940423-c8d8-458c-83b5-2dc7c0a80105', 'application/vnd.oasis.opendocument.text', 'text.png', '4970527f-d0a0-4206-b718-7900c0a80105', '496de976-9a10-403c-aa90-55e6c0a80105', 'odt'),
('49940432-d200-49ff-8ed6-2dc7c0a80105', 'application/vnd.oasis.opendocument.presentation', 'presentation.png', '497052a9-0090-4c06-a483-7900c0a80105', '496de976-9a10-403c-aa90-55e6c0a80105', 'odp'),
('49940453-dbe4-46ba-98ab-42abc0a80105', 'application/vnd.oasis.opendocument.spreadsheet', 'spreadsheet.png', '49705294-bfa8-4ee2-9cec-7900c0a80105', '496de976-9a10-403c-aa90-55e6c0a80105', 'ods'),
('49940538-47ec-44ab-821e-1a65c0a80105', 'application/vnd.oasis.opendocument.text-template', 'text.png', '4970527f-d0a0-4206-b718-7900c0a80105', '496de976-9a10-403c-aa90-55e6c0a80105', 'ott'),
('49940548-aba8-4589-bda5-1a65c0a80105', 'application/vnd.oasis.opendocument.presentation-template', 'presentation.png', '497052a9-0090-4c06-a483-7900c0a80105', '496de976-9a10-403c-aa90-55e6c0a80105', 'otp'),
('4994055f-9600-43a5-a499-287fc0a80105', 'application/vnd.oasis.opendocument.spreadsheet-template', 'spreadsheet.png', '49705294-bfa8-4ee2-9cec-7900c0a80105', '496de976-9a10-403c-aa90-55e6c0a80105', 'ots'),
('4995840d-63d8-4c66-823c-6592c0a80105', 'application/pdf', 'pdf.png', '', '496de908-d92c-4e3b-9447-558fc0a80105', 'pdf'),
('4995845e-0aa4-4677-84d5-66a7c0a80105', 'image/png', 'image.png', '', '496de919-96f4-4c15-8b14-55e6c0a80105', 'png'),
('4bac8218-01d4-4d49-8188-3636c1acb699', 'application/vnd.oasis.opendocument.text-web', 'text.png', '4970527f-d0a0-4206-b718-7900c0a80105', '496de976-9a10-403c-aa90-55e6c0a80105', 'oth'),
('4bac823a-d6d4-4a42-b847-3652c1acb699', 'application/vnd.oasis.opendocument.text-master', 'text.png', '4970527f-d0a0-4206-b718-7900c0a80105', '496de976-9a10-403c-aa90-55e6c0a80105', 'odm');

--
-- Dumping data for table `operatingsystems`
--

INSERT INTO `operatingsystems` (`id`, `platform_id`, `name`, `version`, `codename`, `created`, `modified`) VALUES
('496cb959-3518-4058-97f8-378ec0a80105', '496cb547-0ad8-43e2-9b66-36a5c0a80105', 'Windows', '5.1', 'XP', '2009-01-13 16:55:05', '2009-01-13 16:56:45'),
('496cb965-422c-4d74-b840-378ec0a80105', '496cb547-0ad8-43e2-9b66-36a5c0a80105', 'Windows', '6.0', 'Vista', '2009-01-13 16:55:17', '2009-01-13 16:57:02'),
('496cb98d-8c08-466c-92d3-36a2c0a80105', '496cb541-ddf0-4539-a5a8-36a5c0a80105', 'MacOSX', '10.4', 'Tiger', '2009-01-13 16:55:57', '2009-01-13 16:58:05'),
('496cba26-cc1c-4f28-81cb-36a2c0a80105', '496cb541-ddf0-4539-a5a8-36a5c0a80105', 'MacOSX', '10.5', 'Leopard', '2009-01-13 16:58:30', '2009-01-13 16:58:30'),
('496cba3a-5aa0-4da2-8a49-3748c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Debian GNU/Linux', '4.0', 'Etch', '2009-01-13 16:58:50', '2009-01-13 16:58:50'),
('496cba45-aa48-469a-b6c6-3748c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Debian GNU/Linux', '5.0', 'Lenny', '2009-01-13 16:59:01', '2009-01-13 16:59:01'),
('496cba7f-db30-4c42-9b03-36a1c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Fedora', '8.0', 'Werewolf', '2009-01-13 16:59:59', '2009-01-13 16:59:59'),
('496cba94-dd44-419a-90e3-36a1c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Fedora', '9.0', 'Sulphur', '2009-01-13 17:00:20', '2009-01-13 17:00:20'),
('496cbaa8-6e54-42ef-bfcb-36a5c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Fedora', '10.0', 'Cambridge', '2009-01-13 17:00:40', '2009-01-13 17:00:40'),
('498fec0c-6a18-44a2-a97b-2487c0a80105', '49981c7e-de78-4f11-8cf3-1fcbc0a80105', 'Symbian S60', '3rd Edition', '', '2009-02-09 09:40:44', '2009-03-03 16:14:36'),
('4aeed135-754c-4653-a049-5ea5c0a80105', '496cb547-0ad8-43e2-9b66-36a5c0a80105', 'Windows', '7.0', '', '2009-11-02 13:31:49', '2009-11-02 13:32:13'),
('4aeed16c-9cf4-4cfa-be1e-632dc0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Debian GNU/Linux', '6.0', 'Squeeze', '2009-11-02 13:32:44', '2009-11-02 13:32:59'),
('4aeed1d0-8e54-45ca-a21f-5c62c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Gentoo', '10', '', '2009-11-02 13:34:24', '2009-11-02 13:34:24'),
('4aeed200-17d0-4f57-9fd1-445bc0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Fedora', '11.0', 'Leonidas', '2009-11-02 13:35:12', '2009-11-02 13:35:12'),
('4aeed22b-7e04-4817-b4dd-632dc0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Arch Linux', '2009.02', '', '2009-11-02 13:35:55', '2009-11-02 13:35:55'),
('4aeed23d-1a88-48ed-b8f1-632dc0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Arch Linux', '2009.08', '', '2009-11-02 13:36:13', '2009-11-02 13:36:13'),
('4aeed26f-3608-40a1-b174-5ea5c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'FreeBSD', '7.2', '', '2009-11-02 13:37:03', '2009-11-02 13:37:03'),
('4aeed27c-1d18-4a7a-9f59-5ea5c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'FreeBSD', '6.4', '', '2009-11-02 13:37:16', '2009-11-02 13:37:16'),
('4aeed2a7-9eb0-4de8-8dec-5fc2c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'openSUSE', '10', '', '2009-11-02 13:37:59', '2009-11-02 13:37:59'),
('4aeed2b0-4f4c-4458-b2db-5fc2c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'openSUSE', '11', '', '2009-11-02 13:38:08', '2009-11-02 13:38:08'),
('4aeed2d6-235c-4f42-8d38-17e7c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'MacOSX', '10.6', 'Snow Leopard', '2009-11-02 13:38:46', '2009-11-02 13:38:46'),
('4aeed311-b788-43d0-9839-5ee6c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'SUSE Linux Enterprise', '10', '', '2009-11-02 13:39:45', '2009-11-02 13:39:45'),
('4aeed31c-2220-4761-b66d-5ee6c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'SUSE Linux Enterprise', '11', '', '2009-11-02 13:39:56', '2009-11-02 13:39:56'),
('4aeed336-fd58-4d54-9ebe-5ee6c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Ubuntu', '9.04', 'Jaunty Jackolope', '2009-11-02 13:40:22', '2009-11-02 13:40:22'),
('4aeed343-e514-4b34-bbf5-5ee6c0a80105', '496cb533-ab70-431f-92cc-36a5c0a80105', 'Ubuntu', '9.10', 'Karmic Koala', '2009-11-02 13:40:35', '2009-11-02 13:40:35'),
('4b0d0c41-a970-45bb-b36f-4b11c1acb699', '496cb533-ab70-431f-92cc-36a5c0a80105', 'FreeBSD', '8.0', '', '2009-11-25 11:51:45', '2009-11-25 11:51:45');

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created`, `modified`, `group_id`) VALUES
('4964c464-b090-4907-83d7-619bc0a80105', '*', '2009-01-07 16:04:04', '2009-01-07 16:04:04', '4964c4fc-e7c8-4b21-aef2-6197c0a80105'),
('49b10887-8354-4638-9e46-1c6ac0a80105', 'factories:add', '2009-03-06 12:27:03', '2009-03-06 12:27:03', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('49b1088e-8358-4915-831d-1c6ac0a80105', 'factories:edit', '2009-03-06 12:27:10', '2009-03-06 12:27:10', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('49b10893-cbe4-4516-962e-1c6ac0a80105', 'factories:delete', '2009-03-06 12:27:15', '2009-03-06 12:27:15', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('49b108a0-d414-479a-a26c-1c6ac0a80105', 'workers:index', '2009-03-06 12:27:28', '2009-03-06 12:27:28', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('49b108a4-1848-42d0-8ee5-1c6ac0a80105', 'workers:add', '2009-03-06 12:27:32', '2009-03-06 12:27:32', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('49b108a9-404c-4f19-80ac-1c6ac0a80105', 'workers:edit', '2009-03-06 12:27:37', '2009-03-06 12:27:37', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('49b108af-acdc-4365-892e-1c6ac0a80105', 'workers:delete', '2009-03-06 12:27:43', '2009-03-06 12:27:43', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('49b108b5-9294-4acf-9a89-1c6ac0a80105', 'xmlrpc:api', '2009-03-06 12:27:49', '2009-03-06 12:27:49', '49b10879-6cf8-410e-bbe6-1c6ac0a80105')
('4c10ed1d-3bf8-4d9e-aca8-33abc0a80105', 'jobs:view', '2010-06-10 12:27:49', '2010-06-10 12:27:49', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('4c10ecfb-5044-46a9-b5a6-33abc0a80105', 'jobs:search', '2010-06-10 12:27:49', '2010-06-10 12:27:49', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('4c10ed07-4110-4ae5-9840-33abc0a80105', 'jobs:fail', '2010-06-10 12:27:49', '2010-06-10 12:27:49', '49b10879-6cf8-410e-bbe6-1c6ac0a80105'),
('4c10ed15-6404-49bc-ae5c-33abc0a80105', 'jobs:requeue', '2010-06-10 12:27:49', '2010-06-10 12:27:49', '49b10879-6cf8-410e-bbe6-1c6ac0a80105');

--
-- Dumping data for table `platforms`
--

INSERT INTO `platforms` (`id`, `name`, `order`, `created`, `modified`) VALUES
('496cb533-ab70-431f-92cc-36a5c0a80105', 'Linux/BSD', 1, '2009-01-13 16:37:23', '2009-05-20 14:15:06'),
('496cb541-ddf0-4539-a5a8-36a5c0a80105', 'MacOSX', 3, '2009-01-13 16:37:37', '2009-05-20 14:15:15'),
('496cb547-0ad8-43e2-9b66-36a5c0a80105', 'Windows', 2, '2009-01-13 16:37:43', '2009-05-20 14:15:12'),
('496cb553-75a4-4867-a471-36a5c0a80105', 'Internet', 4, '2009-01-13 16:37:55', '2009-05-20 14:15:15'),
('49981c7e-de78-4f11-8cf3-1fcbc0a80105', 'Mobile', 5, '2009-02-15 14:45:34', '2009-05-20 14:15:08');

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email_address`, `password`, `active`, `created`, `modified`) VALUES
('4be00052-cbe0-4adb-ac41-5b9bc1acb699', 'Administrator', 'admin@example.org', 'e04a8b2f36a774e4b044ec8f9421be408ec0773e', 1, '2010-05-04 13:09:06', '2010-05-04 13:09:06');
