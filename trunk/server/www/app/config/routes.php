<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Connect the root to the Requests controller.
 */
Router::connect('/', array('controller' => 'requests', 'action' => 'add'));
//Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'maintenance'));

/**
 * Connect the 'Pages' controller's urls.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	
/**
 * Connect the 'Lang' controller's urls.
 */
Router::connect('/lang/*', array('controller' => 'lang', 'action' => 'index'));
	
/**
 * Connect the XMLRPC controller properly to the index action. Everything after xmlrpc/ will be passed
 * to the index() action as a parameter. This way a user can browser the API interactively.
 */
Router::connect('/xmlrpc/*', array('controller' => 'xmlrpc', 'action' => 'index'));

?>
