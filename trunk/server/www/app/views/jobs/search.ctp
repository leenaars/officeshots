<div class="jobs form">
	<?php echo $form->create('Job', array('action' => 'search'));?>
		<h3><?php __('Search Job ID');?></h3>
		<?php echo $form->input('id', array('type' => 'text', 'label' => 'Job ID from your factory error message'));?>
	<?php echo $form->submit('Search');?>
</div>
