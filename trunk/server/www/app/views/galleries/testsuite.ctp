<div class="galleries view">
	<h2>
		<?php
			foreach ($path as $parent) {
				echo $html->link($parent['Gallery']['name'], array('action' => 'view', $parent['Gallery']['slug']));
				echo ' / ';
			}
			echo $gallery['Gallery']['name'];
		?>
	</h2>
	<?php echo $gallery['Gallery']['description_html'];?>
</div>

<div id="gallery-results" class="related">
	<h2><?php __('Documents');?></h2>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('File'); ?></th>
		<th><?php __('Validators'); ?></th>
		<?php foreach($applications as $application):?>
			<th>
				<?php
					if ($application['Application']['icon']) {
						echo $html->image('/img/icons/applications/' . $application['Application']['icon']);
						echo '<br />' . $application['Application']['version'];
					} else {
						echo $application['Application']['name'] . '<br />' . $application['Application']['version'];
					}
				?>
			</th>
		<?php endforeach;?>
			<th><?php __('Other');?></th>
		<?php if ($access):?><th class="actions"><?php __('Actions'); ?></th><?php endif;?>
	</tr>
	<?php echo $this->element('testsuite', array('gallery' => $gallery, 'indent' => 0)); ?>
	</table>
</div>

<?php if ($access):?>
	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('Add a document', true), array('action'=>'add_document', $gallery['Gallery']['slug'])); ?> </li>
			<li><?php echo $html->link(__('Edit this gallery', true), array('action'=>'edit', $gallery['Gallery']['slug'])); ?> </li>
			<li><?php echo $html->link(__('Delete this gallery', true), array('action'=>'delete', $gallery['Gallery']['slug']), null, sprintf(__('Are you sure you want to delete "%s"?', true), $gallery['Gallery']['name'])); ?> </li>
		</ul>
	</div>
<?php endif;?>
