<div class="galleries index">
<h2><?php __('Gallery');?></h2>

<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $paginator->sort('name');?></th>
		<th><?php __('Documents'); ?></td>
		<th><?php echo $paginator->sort('Owner', 'user_id');?></th>
	</tr>
	<?php foreach ($galleries as $gallery):?>
		<tr>
			<td>
				<?php echo $html->link($gallery['Gallery']['name'], array('action' => 'view', $gallery['Gallery']['slug'])); ?>
			</td>
			<td>
				<?php echo $gallery['Gallery']['num_documents']; ?>
			</td>
			<td>
				<?php echo $html->link($gallery['User']['name'], array('controller'=> 'users', 'action'=>'view', $gallery['User']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
</table>
</div>

<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Gallery', true), array('action'=>'add')); ?></li>
	</ul>
</div>
