<div class="formats view">
<h2><?php  __('Format');?></h2>
	<dl>
		<dt><?php __('Name'); ?></dt>
		<dd>
			<?php echo $format['Format']['name']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Icon'); ?></dt>
		<dd>
			<?php echo $html->image('icons/' . $format['Format']['icon']); ?>
			&nbsp;
		</dd>
		<dt><?php __('Code'); ?></dt>
		<dd>
			<?php echo $format['Format']['code']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Format', true), array('action'=>'edit', $format['Format']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Format', true), array('action'=>'delete', $format['Format']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $format['Format']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Formats', true), array('action'=>'index')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Mimetypes');?></h3>
	<?php if (!empty($format['Mimetype'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Name'); ?></th>
		<th><?php __('Icon'); ?></th>
		<th><?php __('Extension'); ?></th>
	</tr>
	<?php foreach ($format['Mimetype'] as $mimetype):?>
		<tr>
			<td><?php echo $html->link($mimetype['name'], array('controller'=> 'mimetypes', 'action'=>'view', $mimetype['id'])); ?></td>
			<td><?php echo $mimetype['icon'];?></td>
			<td><?php echo $mimetype['extension'];?></td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
