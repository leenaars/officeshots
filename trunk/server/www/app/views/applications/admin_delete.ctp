<div class="applications form">
<?php echo $form->create('Application', array('action' => 'delete'));?>
	<fieldset>
	<legend><?php __('Delete Application');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('new_id', array(
			'label' => 'Migrate to',
			'type' => 'select',
			'options' => $applications,
			'after' => '<p>Please pick an application to migrate all jobs to.</p>'
		));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
