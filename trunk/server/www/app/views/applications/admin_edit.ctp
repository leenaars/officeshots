<div class="applications form">
<?php echo $form->create('Application');?>
	<fieldset>
		<legend>
		<?php if ($this->action == 'admin_add') {
			__('Add Application');
		} else {
			__('Edit Application');
		}
		?>
		</legend>

	<?php
		if ($this->action == 'admin_edit') {
			echo $form->input('id');
		}
		echo $form->input('name');
		echo $form->input('icon', array(
			'type' => 'select',
			'options' => $images,
			'empty' => '(none)',
			'after' => '<p>Upload images to app/webroot/img/icons/applications. Roughly 75x75px with a <em>transparent</em> background.</p>'
		));
		echo $form->input('Doctype');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
