<div class="doctypes form">
<?php echo $form->create('Doctype');?>
	<fieldset>
		<legend>
		<?php if ($this->action == 'admin_edit') {
			__('Edit Doctype');
		} else {
			__('Add Doctype');
		}
		?>
		</legend>
	<?php
		if ($this->action == 'admin_edit') {
			echo $form->input('id');
		}
		echo $form->input('name');
		echo $form->input('code');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
