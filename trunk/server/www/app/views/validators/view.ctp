<div class="requests view">
<h2><?php
	echo $validatorModel->getStateIcon($validator) . ' ';

	if (isset($validator['Result']['Job'])) {
		$link = $html->link($validator['Request']['filename'], array('controller' => 'results', 'action' => 'view', $validator['Result']['id']));
	} else {
		$link = $html->link($validator['Request']['filename'], array('controller' => 'requests', 'action' => 'view', $validator['Request']['id']));
	}

	printf(__('File "%s"', true), $link);
?></h2>
<?php
	if (isset($validator['Result']['Job'])) {
		printf(__('<p>Rendered by %s %s (%s)</p>', true),
			$validator['Result']['Job']['Application']['name'],
			$validator['Result']['Job']['version'],
			$validator['Result']['Job']['Platform']['name']);
	}
?>


<dl style="margin-left: 10em;">
	<dt><?php __('Validator'); ?></dt>
	<dd><?php echo $validator['Validator']['name'];?></dd>
	<dt><?php __('Result'); ?></dt>
	<dd><?php echo $validatorModel->getState($validator, false);?></dd>
	<dt><?php __('Raw output'); ?></dt>
	<dd><?php echo nl2br($validator['Validator']['response']);?></dd>
</dl>

</div>
