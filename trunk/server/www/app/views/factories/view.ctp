<div class="factories view">
<h2><?php  printf(__('Factory "%s"', true), $factory['Factory']['name']);?></h2>
	<dl>
		<dt><?php __('Owner'); ?></dt>
		<dd>
			<?php echo $html->link($factory['User']['name'], array('controller'=> 'users', 'action'=>'view', $factory['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php __('Operatingsystem'); ?></dt>
		<dd>
			<?php echo $factory['Operatingsystem']['name']; ?>
			<?php echo $factory['Operatingsystem']['version']; ?>
			<?php if ($factory['Operatingsystem']['codename']): ?>
				(<?php echo $factory['Operatingsystem']['codename']; ?>)
			<?php endif; ?>
			&nbsp;
		</dd>
		<dt><?php __('Hardware'); ?></dt>
		<dd>
			<?php echo $factory['Factory']['hardware']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Active since'); ?></dt>
		<dd>
			<?php echo $factory['Factory']['active_since']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Last activity'); ?></dt>
		<dd>
			<?php echo $factory['Factory']['last_poll']; ?>
			&nbsp;
		</dd>
	</dl>
	<?php if ($canEdit): ?>
		<p><?php echo $html->link(__('Edit this factory', true), array('controller'=> 'factories', 'action'=>'edit', $factory['Factory']['id']));?> </li>
	<?php endif; ?>
</div>
<div class="related">
	<h3><?php __('Installed office applications');?></h3>
	<?php if (!empty($factory['Worker'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Application'); ?></th>
		<th><?php __('Version'); ?></th>
		<th><?php __('Development'); ?></th>
		<th><?php __('Output formats'); ?></th>
		<th><?php __('Use for test suites'); ?></th>
		<?php if ($canEdit): ?>
			<th class="actions"><?php __('Actions');?></th>
		<?php endif; ?>
	</tr>
	<?php foreach ($factory['Worker'] as $worker):?>
		<tr>
			<td><?php echo $worker['Application']['name'];?></td>
			<td><?php echo $worker['version'];?></td>
			<td><?php if ($worker['development']) { __('Unstable'); } else { __('Stable'); } ?></td>
			<td><?php echo implode(', ', Set::extract('/Format/name', $worker));?></td>
			<td><?php echo $worker['testsuite'] ? __('Yes', true) : __('No', true);?></td>
			<?php if ($canEdit): ?>
				<td class="actions">
					<?php echo $html->link(__('Edit', true), array('controller'=> 'workers', 'action'=>'edit', $worker['id'])); ?>
					<?php echo $html->link(__('Delete', true), array('controller'=> 'workers', 'action'=>'delete', $worker['id']), null, sprintf(__('Are you sure you want to delete this instance of %s from this factory?', true), $worker['Application']['name'])); ?>
				</td>
			<?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<?php if ($canEdit): ?>
		<p><?php echo $html->link(__('Add an application', true), array('controller'=> 'workers', 'action'=>'add', $factory['Factory']['id']));?> </li>
	<?php endif; ?>
</div>
