<div class="factories form">
<?php echo $form->create('Factory');?>
	<fieldset>
		<legend>
			<?php if ($this->action == 'add' || $this->action == 'admin_add') {
				__('Add a new factory');
			} else {
				__('Edit this factory');
			}
			?>
		</legend>
		<?php
			if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
				echo $form->input('user_id');
			}
			echo $form->input('name');
			echo $form->input('hardware');
		?>
		<div class="input select">
			<label for="FactoryOperatingsystemId">Operatingsystem</label>
			<select name="data[Factory][operatingsystem_id]" id="FactoryOperatingsystemId">
			<?php foreach ($operatingsystems as $os): ?>
				<option
					value="<?php echo $os['Operatingsystem']['id'];?>"
					<?php if ($this->data && $os['Operatingsystem']['id'] == $this->data['Factory']['operatingsystem_id']) {echo 'selected="selected"';} ?>
				>
					<?php echo $os['Operatingsystem']['name']; ?>
					<?php echo $os['Operatingsystem']['version']; ?>
					<?php if ($os['Operatingsystem']['codename']): ?>
						(<?php echo $os['Operatingsystem']['codename']; ?>)
					<?php endif; ?>
					&nbsp;
				</option>
			<?php endforeach; ?>
			</select>
		</div>
		<?php
			if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
				echo $form->input('last_poll', array('empty' => true));
				echo $form->input('active_since', array('empty' => true));
			}
		?>
	</fieldset>
	<?php echo $form->end('Submit');?>
</div>
