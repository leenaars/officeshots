<div class="platforms view">
<h2><?php  __('Platform');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $platform['Platform']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $platform['Platform']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Order'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $platform['Platform']['order']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $platform['Platform']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $platform['Platform']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Platform', true), array('action'=>'edit', $platform['Platform']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Platform', true), array('action'=>'delete', $platform['Platform']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $platform['Platform']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Platforms', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Platform', true), array('action'=>'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Operatingsystems');?></h3>
	<?php if (!empty($platform['Operatingsystem'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Name'); ?></th>
		<th><?php __('Version'); ?></th>
		<th><?php __('Codename'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($platform['Operatingsystem'] as $operatingsystem):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $operatingsystem['name'];?></td>
			<td><?php echo $operatingsystem['version'];?></td>
			<td><?php echo $operatingsystem['codename'];?></td>
			<td><?php echo $operatingsystem['created'];?></td>
			<td><?php echo $operatingsystem['modified'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller'=> 'operatingsystems', 'action'=>'view', $operatingsystem['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller'=> 'operatingsystems', 'action'=>'edit', $operatingsystem['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller'=> 'operatingsystems', 'action'=>'delete', $operatingsystem['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $operatingsystem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Operatingsystem', true), array('controller'=> 'operatingsystems', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
