<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The formats controller
 */
class FormatsController extends AppController {

	var $name = 'Formats';
	var $helpers = array('Html', 'Form');

	function _get_icons() {
		$icons = glob(IMAGES . 'icons/*.*');
		foreach ($icons as &$icon) {
			$icon = basename($icon);
		}
		$icons = array_combine($icons, $icons);
		return $icons;
	}

	function admin_index() {
		$this->Format->recursive = 0;
		$this->set('formats', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Format.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('format', $this->Format->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Format->create();
			if ($this->Format->save($this->data)) {
				$this->Session->setFlash(__('The Format has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Format could not be saved. Please, try again.', true));
			}
		}

		$this->render('admin_edit');
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Format', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Format->save($this->data)) {
				$this->Session->setFlash(__('The Format has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Format could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Format->read(null, $id);
		}

		$this->set('icons', $this->_get_icons());
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Format', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Format->del($id)) {
			$this->Session->setFlash(__('Format deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
