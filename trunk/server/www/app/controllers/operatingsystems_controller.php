<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The operatingsystems controller
 */
class OperatingsystemsController extends AppController {

	var $name = 'Operatingsystems';
	var $helpers = array('Html', 'Form');

	function admin_index() {
		$this->Operatingsystem->recursive = 0;
		$this->set('operatingsystems', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Operatingsystem.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('operatingsystem', $this->Operatingsystem->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Operatingsystem->create();
			if ($this->Operatingsystem->save($this->data)) {
				$this->Session->setFlash(__('The Operatingsystem has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Operatingsystem could not be saved. Please, try again.', true));
			}
		}
		$platforms = $this->Operatingsystem->Platform->find('list');
		$this->set(compact('platforms'));
		$this->render('admin_edit');
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Operatingsystem', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Operatingsystem->save($this->data)) {
				$this->Session->setFlash(__('The Operatingsystem has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Operatingsystem could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Operatingsystem->read(null, $id);
		}
		$platforms = $this->Operatingsystem->Platform->find('list');
		$this->set(compact('platforms'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Operatingsystem', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Operatingsystem->del($id)) {
			$this->Session->setFlash(__('Operatingsystem deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
