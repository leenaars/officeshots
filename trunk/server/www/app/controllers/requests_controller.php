<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Requests controller
 */
class RequestsController extends AppController
{
	/** @var array The components this controller uses */
	public $components = array('AuthCert', 'RequestHandler');
	
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form', 'Javascript', 'RequestModel', 'ValidatorModel');

	/** @var array Add Request and Worker model */
	public $uses = array('Request', 'Worker', 'Mimetype', 'User', 'Format');

	/** @var array Set default sort order for paginate */
	public $paginate = array(
		'order' => array('Request.created' => 'desc')
	);

	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->AuthCert->allow('add', 'view', 'download');
	}

	/**
	 * List all your requests
	 * @return void
	 */
	public function index()
	{
		$this->set('requests', $this->paginate('Request', array('Request.user_id' => $this->AuthCert->user('id'))));
	}

	/**
	 * Get a Request by the request ID and check access control
	 *
	 * @param string $id The request ID
	 * @param $type the access type, either 'read' or 'write'
	 * @return array An array containing the request
	 */
	private function _getRequest($id, $type = 'read')
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Request.', true));
			$this->redirect(array('action'=>'add'));
		}
		
		$this->Request->contain(array(
			'User',
			'Mimetype',
			'Mimetype.Doctype',
			'Job',
			'Job.Format',
			'Job.Platform',
			'Job.Application',
			'Job.Result',
			'Job.Result.Mimetype',
			'Job.Result.Format',
			'Job.Result.Validator' => array('order' => 'Validator.name ASC'),
			'Gallery',
			'Validator' => array('order' => 'Validator.name ASC'),
		));

		$request = $this->Request->read(null, $id);
		if (empty($request) || !$this->Request->checkAccess($this->AuthCert->user('id'), $type, $id)) {
			$this->Session->setFlash(__('Invalid Request.', true));
			$this->redirect(array('action'=>'add'));
		}

		usort($request['Job'], array($this, '_cmpJobs'));
		return $request;
	}

	/**
	 * usort() callback to properly sort Jobs
	 */
	private function _cmpJobs($a, $b)
	{
		if (0 !== ($cmp = strcmp($a['Application']['name'], $b['Application']['name']))) {
			return $cmp;
		}

		if (0 !== ($cmp = strcmp($a['version'], $b['version']))) {
			return $cmp;
		}

		if (0 !== ($cmp = strcmp($a['Platform']['name'], $b['Platform']['name']))) {
			return $cmp;
		}

		$format_a = ($a['Result'] ? $a['Result']['Format']['name'] : ($a['Format'] ? $a['Format']['name'] : ''));
		$format_b = ($b['Result'] ? $b['Result']['Format']['name'] : ($b['Format'] ? $b['Format']['name'] : ''));

		if (0 !== ($cmp = strcmp($format_a, $format_b))) {
			return $cmp;
		}

		return strcmp($a['created'], $b['created']);
	}

	/**
	 * View a single request
	 *
	 * @param string $id The request ID
	 * @return void
	 */
	public function view($id = null)
	{
		// Used to display the jobs associated with the Request
		$this->helpers[] = 'JobModel';

		$request = $this->_getRequest($id, 'read');
		$writeAccess = $this->Request->checkAccess($this->AuthCert->user('id'), 'write', $id);

		$this->set(compact('request', 'writeAccess'));
	}

	/**
	 * Edit a request description
	 *
	 * @param string $id The request ID
	 * @return void
	 */
	public function edit($id = null)
	{
		$request = $this->_getRequest($id, 'write');

		if (!empty($this->data)) {
			$this->data['Request']['id'] = $id;
			if (!$this->Request->save($this->data)) {
				$this->Session->setFlash(__('Unable to save the description', true));
			} else {
				$this->redirect(array('action' => 'view', $id));
			}
		}

		$this->set(array(
			'request' => $request,
		));
	}

	/**
	 * Add a new request
	 * @return void
	 */
	public function add()
	{
		if (!empty($this->data)) {
			// Access control
			if (!Configure::read('Auth.allowAnonymous') && !$this->AuthCert->user()) {
				$this->Session->setFlash(__('You are not allowed to submit requests anonymously.', true));
				$this->redirect(array('action'=>'add'));
			}

			// Check the daily request limits
			if ($user = $this->AuthCert->user()) {
				$requestLimit = Configure::read('Request.limitRegistered');

				$user = $this->User->find(array('User.id' => $user['User']['id']));
				$limits = Set::extract('/Group/request_limit', $user);
				$limits[] = $requestLimit;
				$requestLimit = max($limits);

				$numRequests = $this->Request->find('count', array(
					'conditions' => array(
						'Request.user_id' => $user['User']['id'],
						'Request.created >' => date('Y-m-d H:i:s', strtotime('-1 day'))
					)
				));

				if ($numRequests > $requestLimit) {
					$this->Session->setFlash(sprintf(__('You have exceeded your quota of %d daily requests.', true), $requestLimit));
					$this->redirect(array('action'=>'add'));
				}
			} else {
				$requestLimit = Configure::read('Request.limitAnonymous');
				$numRequests = $this->Request->find('count', array(
					'conditions' => array(
						'Request.ip_address' => inet_ptod($this->RequestHandler->getClientIP()),
						'Request.created >' => date('Y-m-d H:i:s', strtotime('-1 day'))
					)
				));

				if ($numRequests > $requestLimit) {
					$this->Session->setFlash(sprintf(__('You have exceeded your quota of %d daily requests.', true), $requestLimit));
					$this->redirect(array('action'=>'add'));
				}
			}

			// Set some extra data
			$this->data['Request']['user_id']    = (string) $this->AuthCert->user('id');
			$this->data['Request']['ip_address'] = inet_ptod($this->RequestHandler->getClientIP());
			$this->data['Request']['expire']     = date('Y-m-d H:i:s', time() + Configure::read('Request.expire'));
			$this->data['Request']['state']      = Request::STATE_PREPROCESSOR_QUEUED;


			// Create the request
			$this->Request->create();
			if (!$this->Request->save($this->data)) {
				$this->Session->setFlash(__('The request could not be saved', true));
				$this->redirect(array('action'=>'add'));
			}

			// Set root and path based on the ID
			$this->Request->read();
			$this->Request->set(array(
				'root' => 'requests' . DS . $this->Request->id,
				'path' => 'requests' . DS . $this->Request->id . DS . 'source',
			));
			
			// Add the file upload
			$errors = array();
			if (!$this->Request->addUpload($this->data, $errors)) {
				$this->Request->delete();
				$this->Session->setFlash(implode("<br />\n", $errors));
				$this->redirect(array('action'=>'add'));
			}

			// Add the jobs to the request
			$this->Request->read();
			if (isset($this->data['Request']['App'])) {
				$jobs = array();
				$format_id = $this->data['Request']['format_id'];
				foreach ($this->data['Request']['App'] as $app) {
					list($platform_id, $doctype_code, $application_id, $version) = explode('_', $app);
					$jobs[] = compact('platform_id', 'application_id', 'version', 'format_id');
				}

				$this->Request->addJobs($jobs);
			}

			// Add ODF Validators
			$this->Request->addValidators();

			// Queue the preprocessor
			if (!$this->Request->defer('run', 'Preprocessor')) {
				$this->Request->delete();
				$this->Session->setFlash(__('Failed to queue the request for the virus scanner. Please contact system administration.', true));
				$this->redirect(array('action'=>'add'));
			}

			// All done!
			$this->redirect(array('action' => 'view', 'id' => $this->Request->id));
		}

		$this->set('can_have_factories', $this->__permitted('factories', 'edit'));
		$this->set('can_submit_requests', ($this->__permitted('factories', 'edit') || $this->AuthCert->user()));
		
		// TODO: The result of all the below actions should really be cached for better performance
		$workers = $this->Worker->getActive();

		// Give all the active workers a unique, short ID.
		// We need this for checking and unchecking sets of applications
		$count = 0;
		foreach ($workers as &$worker) {
			$worker['short_id'] = 'c'.$count++;
		}
		unset($worker); // Don't leave it assigned or bugs will occur below

		// Get all the other information we need to build the front page
		$platforms = $this->Worker->Factory->Operatingsystem->Platform->find('all', array('recursive' => -1));
		$doctypes = $this->Worker->Application->Doctype->find('all', array('recursive' => -1));
		$formats = $this->Format->find('list', array('recursive' => -1));
		$mimetypes = $this->Mimetype->find('all', array('recursive' => -1));

		// Build a format map which consists of sets of active applications
		$sets = array(
			'platform'    => array(),
			'extension'   => array(),
			'format'      => array(),
			'latest'      => array(),
			'development' => Set::extract('/Worker[development=1]/../short_id', $workers),
			'all'         => Set::extract('/short_id', $workers)
		);

		foreach ($platforms as $platform) {
			$sets['platform'][$platform['Platform']['id']] = Set::extract('/Platform[id=' . $platform['Platform']['id'] . ']/../short_id', $workers);
		}
		foreach ($mimetypes as $mimetype) {
			if ($mimetype['Mimetype']['doctype_id']) {
				$sets['extension'][$mimetype['Mimetype']['extension']] = Set::extract('/Doctype[id=' . $mimetype['Mimetype']['doctype_id'] . ']/../short_id', $workers);
			}
		}
		foreach ($formats as $format_id => $format) {
			$sets['format'][$format_id] = Set::extract('/Format[id=' . $format_id . ']/../short_id', $workers);
		}

		// This is the tricky bit. Set the highest version for every application/platform combo as default
		// This requires the workers array to be sorted by platform, app, version
		$short_id = false;
		$platform_id = '';
		$application_id = '';
		$doctype_id = '';
		foreach ($workers as $worker) {
			if ($worker['Worker']['development']) {
				continue;
			}

			if ($worker['Application']['id'] != $application_id || $worker['Platform']['id'] != $platform_id || $worker['Doctype']['id'] != $doctype_id) {
				if ($short_id) {
					$sets['latest'][] = $short_id;
				}

				$platform_id = $worker['Platform']['id'];
				$application_id = $worker['Application']['id'];
				$doctype_id = $worker['Doctype']['id'];
			}

			$short_id = $worker['short_id'];
		}
		$sets['latest'][] = $short_id; // don't forget to add the last one.

		$this->set(compact('workers', 'platforms', 'doctypes', 'formats', 'sets', 'mimetypes'));
	}

	/**
	 * Download a request
	 *
	 * @param string $id The request ID
	 */
	public function download($id = null)
	{
		$request = $this->_getRequest($id, 'read');
		extract($this->Request->createZip());
		
		$this->view = 'Media';
		$this->set(array(
			'id' => $id,
			'name' => $filename,
			'download' => true,
			'extension' => 'zip',
			'path' => $directory . DS,
		));
	}

	/**
	 * Extend the deadline of a request
	 *
	 * @param string $id The request ID
	 */
	public function extend($id = null)
	{
		$request = $this->_getRequest($id, 'write');

		if (strtotime($request['Request']['expire']) < time()) {
			$this->Session->setFlash(__('You cannot extend a request that has expired.', true));
			$this->redirect(array('action' => 'view', $id));
		}

		$this->Request->saveField('expire', date('Y-m-d H:i:s', time() + Configure::read('Request.expire')));
		$this->redirect(array('action' => 'view', $id));
	}

	/**
	 * Cancel a job by setting it's expiration time
	 *
	 * @param string $id The request ID
	 */
	public function cancel($id = null)
	{
		$request = $this->_getRequest($id, 'write');
		$this->Request->cancel();
		$this->redirect(array('action' => 'view', $id));
	}

	/**
	 * List all requests
	 * @return void
	 */
	public function admin_index()
	{
		$this->Request->recursive = 0;
		$this->paginate = array('order' => 'Request.created');
		$this->set('requests', $this->paginate());
	}

	/**
	 * View a single request
	 *
	 * @param string $id The request ID
	 * @return void
	 */
	public function admin_view($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Request.', true));
			$this->redirect(array('action'=>'index'));
		}

		// Used to display the jobs associated with the Request
		$this->helpers[] = 'JobModel';

		$request = $this->_getRequest($id, 'admin');
		$this->set(compact('request'));
	}

	/**
	 * Edit a request
	 *
	 * @param string $id The request ID
	 * @return void
	 */
	public function admin_edit($id = null)
	{
		$request = $this->_getRequest($id, 'admin');

		if (!empty($this->data)) {
			$this->data['Request']['id'] = $id;
			if (!$this->Request->save($this->data)) {
				$this->Session->setFlash(__('Unable to save the description', true));
			} else {
				$this->redirect(array('action' => 'view', $id));
			}
		}

		$this->set(array(
			'request' => $request,
		));
		$this->render('edit');
	}

	/**
	 * Delete a request
	 *
	 * @param string $id The request ID
	 * @return void
	 */
	public function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Request', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Request->del($id)) {
			$this->Session->setFlash(__('Request deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}

?>
