<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The controller for the testsuites
 */
class TestsuitesController extends AppController {

	var $name = 'Testsuites';
	var $helpers = array('Html', 'Form');

	function admin_index() {
		$this->Testsuite->recursive = 0;
		$this->set('testsuites', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Testsuite.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('testsuite', $this->Testsuite->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Testsuite->create();
			if ($this->Testsuite->save($this->data)) {
				$this->Session->setFlash(__('The Testsuite has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Testsuite could not be saved. Please, try again.', true));
			}
		}

		$this->render('admin_edit');
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Testsuite', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Testsuite->save($this->data)) {
				$this->Session->setFlash(__('The Testsuite has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Testsuite could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Testsuite->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Testsuite', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Testsuite->del($id)) {
			$this->Session->setFlash(__('Testsuite deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>
